\id HAG
\h Ageo
\toc1 Buku ya Profeta Ageo
\toc2 Ageo
\toc3 Ag
\mt1 Buku ya Profeta Ageo
\imt Boyingisi
\is1 Mokomi
\ip Profeta Ageo, bayebi ye se na nkombo oyo elakisi ’te « oyo abotami o mokolo mwa feti ». Bileko mpe esika ya mbotama ya ye eyebani te. Bosangani bwa Buku ya Mibeko mpe nsango ya Ezekia, ezali kolakisa ’te boyambi ezalaki makasi o mibu mya boombo, azongaki na liboke lya bato o Yeruzalem. 
\is Eleko
\ip Soki totali makambo maye malandi eleko ya ebandela ya Buku ya Profeta Ageo (1,1 el), mosala mwa profeta mola­ndi nzela ya eleko eye ezalaka o kati ya sanza ya muambe mpe ya zomi na mibale ya mobu 520. Ezali o sanza eye bato­ngi lisusu Tempelo ya Yeruzalem, na makanisi ma profeta. Bomekami bwa liboso bozalaka o mobu 537 (Esd 3,7–12). Profeta Ageo alendisi mpiko ya bokonzi bwa ba-Yuda baye bautaki o boombo o Babilon mpe bazwaki ndingisa ya mokonzi wa Persi, kozonga o mboka Yeruzalem. Bandeko baye bazongi o mboka boya­mbi bwa bango bolendi o eleko ya boo­mbo, bazwi na profeta Ezekia ’te base­ngeli kosambela na solo o kati ya Tempelo ya sika. Basengelaki koyeba ’te botongi Tempelo to esika esantu lokola ngomba Sion. Kasi bakutani na mikakatano mi­ngi mpenza mpo ya bato mpe misolo mpo ya misala. Baye bazongaka liboso bazalaka na motuya mwa 50 000 mpe esengelaki ’te moko moko akanisa ndenge ya kofanda. Lisa­nga lya bato bazali koloba ’te : « Ntango ya kotongo Tempelo lisusu ekoki naino te » (Ag 1,2).
\is Mwango
\ip Buku ya Profeta Ageo ezali na masolo matano maye mazali ndenge boye : 
\ip Eteni 1,1–14 : profeta asengi lisanga batonga Tempelo ya sika. 
\ip Eteni 1, 15 a ; 2,15–19 : botongi Tempelo oyo ebandisi eleko ya libiki. 
\ip Eteni 1,15 b-2,9 : nkembo ya ndako eye ya sika ekoleka nkembo ya ndako ya kala. 
\ip Eteni 2,10–14 : se bato bazali na loboko la solo epai ya Yawe nde bandimami mpo ya kosala mosala mwa botongi Tempelo. 
\ip Eteni 2,20–23 : Yawe alakeli Zorobabel. 
\is Nsango
\ip Profeta Ageo ameki kolakisa na bato ba ntango eye bilembo bya eleko : bobola, bi­langa bikobota mbuma mingi te oyo ezali na etumbu ya Nzambe na maye matali boyambi bwa libota. Profeta ase­ngi na bango bamiyingisa o kati ya eli­kya ya makasi mpe kotiya o mosala mwa botongi ndako ya ntalo mpo ya Mokonzi. Bongo bobenisi bwa likolo bokobota mbuma mpe bokofongola eleko ya libiki lya seko. Nsango ya profeta Ageo ezali nzeneneke ya boyambi o kati ya bozeli bokonzi bwa Nzambe, boye bokangemi mpe botingami o nzela ya libiki oyo ekokota o kati ya mokili. Tempelo mpe bokonzi bwa Kristu, bokokani na Zorobabel, moponami wa libota lya Davidi, oyo Nza­mbe atiyi o liboso lya libota lya ye.
\c 1
\s1 Batonga Tempelo ya sika
\p
\v 1 O mobu mwa mibale mwa bokonzi bwa Dario *, o sanza ya motoba, o mokolo mwa yambo mwa sanza, Yawe atindeli profeta Ageo maloba. Atindi ye epai ya Zorobabel *, mwana wa Sealtiel, moyangeli wa Yuda, mpe epai ya Yozue, mwana wa Yosadak, nganga mokonzi, ayebisa bango ’te :
\v 2 Yawe wa bokasi bonso alobi boye : Bato baye balobi : « Ntango ya kotonga Tempelo lisusu ekoki naino te * ! »
\v 3 Kasi Yawe atuni bango na monoko mwa profeta Ageo :
\v 4 Ebongi solo ’te bino bofanda o ndako ikembami na mabaya, awa Tempelo ebebi nye ?
\v 5 Yawe wa bokasi bonso alobi boye : Bokanisa malamu nini bosengeli ko­sala.
\v 6 Bolonaki mboto mingi, kasi bobukaki moke. Boliaki mingi, kasi botondaki te. Bomelaki mingi, kasi bosilisaki mposa te. Bolataki bilamba mingi, kasi boyokaki se mpio. Botiaki lifuta lya mosala o libenga litubwana !
\v 7 Yawe wa bokasi bonso alobi boye : Bokanisa malamu nini bosengeli kosala !
\v 8 Bobuta ngomba mpe bóya na mabaya, botonga lisusu ndako ya ngai, bongo nasepela mpe nalakisa nkembo ya ngai wana. Maloba ma Yawe.
\v 9 Bozalaki kolikya ’te bokozwa mbuma mingi, kasi bozwaki se moke. Maye bokotisi o ebombelo, ngai napemeli mango mpema. Ntina nini nasalaki bongo ? Yawe wa bokasi bonso alobi boye : Nasalaki bongo mpo ndako ya ngai ebebi nye. Bokokanisaka se makambo ma bino moko, moto na moto makambo ma ndako ya ye.
\v 10 Yango wana o likolo naka­ngaki mbula mpe mabelé moboti te.
\v 11 Nasali ’te binso bikauka : bila­nga, ngomba, mbuma ya mampa, nzete ya vino iboti sika, mafuta ma sika, na milona misusu minso ; bato na bibwele bazanga mai, na misala mya bango minso.
\v 12 Zorobabel, mwana wa Sealtiel, na nganga mokonzi Yozue, mwana wa Yosadak, na bato banso bayoki maloba ma Yawe Nzambe wa ba­ngo, bandimi maloba ma profeta Ageo. Ageo alobaki lokola Yawe Nzambe wa bango atindaki ye, mpe bato banso babangi Yawe.
\v 13 Ageo, ntoma wa Yawe, alobi na bato se lokola Yawe atindaki ye : « Nazali na bino - Maloba ma Yawe. »
\v 14 Yawe alendisi bongo Zorobabel, mwa­na wa Sealtiel, moyangeli wa Yuda ; alendisi mpe nganga mokonzi Yozue, mwana wa Yosadak, na mpe mitema mya bato banso *. Bayei mpe babandi mosala o Tempelo ya Yawe wa bokasi bonso, Nzambe wa bango.
\v 15 Ezalaki sanza ya motoba, mokolo mwa ntuku ibale na minei.
\c 2
\s1 Nkembo ya Tempelo
\p
\v 1 O mobu mwa mibale mwa bokonzi bwa Dario, sanza ya nsa­mbo, o mokolo mwa ntuku ibale na moko, Yawe atindi profeta Ageo boye :
\v 2 Loba na Zorobabel, mwana wa Sealtiel, moyangeli wa Yuda, mpe na nganga mokonzi Yozue, mwana wa Yosadak, mpe na bato banso :
\v 3 Nani o kati ya bino amonoki ndako na nkembo lokola ezalaka kala ? Sikawa bomoni ’te ekomi nde­nge nini ? Bomoni te ’te ekomi mpamba * ?
\v 4 Yawe alobi boye : Yo Zorobabel, lendisa motema ! Yo nganga mokonzi Yozue, mwana wa Yosadak, zala na mpiko ! Bino bana mboka banso, bopika mpende ! Maloba ma Yawe. Botia maboko o mosala ! Ngai nazali na bino. Maloba ma Yawe wa bokasi bonso.
\v 5 Eza­li bondeko nakataki na bino, nta­ngo bobimaki o Ezipeti : Elimo ya ngai ezali o kati ya bino, bobanga te !
\v 6 Yawe wa bokasi bonso alobi boye : Etikali moke naningisa likolo na nse, mbu na mokili.
\v 7 Nakoni­ngisa bikolo binso ; nkita ya bikolo binso ikokoma awa. Nakotondisa Tempelo eye na biloko bya lokumu. Maloba ma Yawe wa bokasi bonso.
\v 8 Yawe wa bokasi bonso alobi : Ngai nkolo wa palata mpe wa wolo !
\v 9 Nkembo ya ndako eye ya sika ekoleka nkembo ya ndako ya kala. Nakotia boboto o ndako eye. Maloba ma Yawe wa bokasi bonso.
\s1 Batuni banganga Nzambe *
\p
\v 10 O mobu mwa mibale mwa bokonzi bwa Dario, sanza ya libwa, o mokolo mwa ntuku ibale na minei, Yawe alobi na profeta Ageo maloba maye :
\v 11 Yawe wa bokasi bonso alobi : Tuna banganga Nzambe, bayebisa yo boniboni bakokata likambo liye :
\v 12 « Soko moto amemi nyama ya libonza o nsonge ya elamba, mpe asimbi na yango mampa, ndu­nda, vino, mafuta to bilei bisusu, bilei bina bikokoma nde bisantu ? » Banganga Nzambe bayanoli ye : « Bikokoma bilei bisantu te ! »
\v 13 Ageo atuni lisusu : « Moto akomi na mbi­ndo mpo asimbaki ebembe, soko asimbi mpe biloko biye totangi, biloko biye bikokoma nde na mbi­ndo ? » Banganga Nzambe bayanoli : « Biloko bina bikozala na mbindo ! »
\v 14 Ageo ayanoli : « Yawe alobi : Ezali se bongo na bato ba ngai ! Maka­mbo bato ba ekolo eye bakosalaka o miso ma ngai, se bongo : biye bokobonzaka awa, bizali na mbindo. »
\s1 Bilanga bikobota mbuma mingi
\p
\v 15 Bokeba na maye makosalema ba­nda lelo : Liboso ’te batonga Tempelo ya Yawe,
\v 16 naino batiaki mabanga manene te, bato bayei kotala etutelo ya mbuma ya mampa, bakanisaki ’te bakozwa mabenga ntuku ibale, kasi bazwi se zomi. Bayaki kotala ekamelo ya vino, bakanisaki ’te bakozwa bingwongolo ntuku itano, kasi bazwi se ntuku ibale.
\v 17 Napesaki bino etumbu o misala minso bosali na maboko ma bino. O bilanga mbuma izwi bokono mpe bipoli, mbula ya mata­ndala ebebisi yango, kasi bino bozo­ngeli ngai te ! Maloba ma Yawe.
\v 18 Ba­nda lelo mpe mikolo minso mikoya, bo­tala maye makosalema : banda sanza ya libwa, mokolo mwa ntuku ibale na minei, banda mokolo babandaki kotonga lisusu Tempelo ya Yawe, bokeba na mango !
\v 19 Mbuma ya mampa ezali o ebombelo ? Ata nzete ya vino, ya figi, ya ndimo mpe ya oliva eboti te. Kasi banda lelo, ngai nakobenisa yango.
\s1 Yawe alakeli Zorobabel *
\p
\v 20 Mbala ya ibale Yawe alobi na Ageo ; ezalaki o mokolo mwa ntuku ibale mwa sanza.
\v 21 Atindi ye boye : Loba na Zorobabel, moyangeli wa Yuda : nakoningisa likolo na nse.
\v 22 Nakokweisa ngwende ya bako­nzi ; nakosilisa bokasi bwa bato ba bikolo ; nakokweisa makalo na baye bakotambwisaka mango ; farasa na basoda ba yango bakokufa na mopanga mwa baninga.
\v 23 Yawe wa bokasi bonso alobi boye : O mokolo mona nakopono yo Zorobabel, mwana wa Sealtiel, mosaleli wa ngai. Maloba ma Yawe. Nakopesa yo bokonzi mpe nakotia yo mpete * o mosapi seko, mpo ngai naponi yo. Maloba ma Yawe wa bokasi bonso.
