MAKABE 1

1,15. Mpo ya Mobeko mwa Moze na mimeseno mya bankoko ba-Yuda baka-bwanaki na mikili misusu, miye Alesandro asangisaki o bokonzi boko. Bizalela bya bato baike ba mikili mina bibebaki mingi. Bobele na boyambi bwa solo ba-Yuda bakokaki kobatela mambi ma Nzambe bankoko batikela bango.
1,54. Ekeko eye ya nsomo etangemi mpe o Dan 9,27 ; 11,31. Na maloba maye balakisi altare eye bapagano batongaki likolo lya altare ya Tempelo ya ba-Yuda. ekokoma na ntina esusu.
2,42. Bato ba lingomba lya ba-Kasideo balingaki kobongisa mambi ma Nzambe. Lingomba lya ba-Farizeo libimi nsima ya bango. Na nsima mangomba maye mabale masangani, kasi makokabwana lisusu, ntango bana ba Matatias bakomi kosala makambo ma politiki.
3,48. Mpo profeta moko azali te, bafungoli buku ya Mobeko mpo ’te bayeba nini Nzambe alingi : bakende etumba to te. O Biblia mobimba ekomami se mbala yoko ’te ba-Yuda basali lokola bapagano. Bango bameseni kotuna Liloba lya Nza­mbe.
5,55 Yozefu na Azarya bakolonga etumba te, mpo balukaki bolamu bwa bango moko ; bapangani te na maye Nzambe alingi.
6,1. Liwa lya Antioko lilakisami bo ndakisa ya nsuka ya banyokoli.
6,55. Bitumba bisili ntango ba-Yuda balingi babwaka elikya. Mpo ya mpiko ya mwa liboke lya bato, ba-Yuda bako­zwa ndingisa ya kosambela Yawe na bo­nsomi bonso.
8,1. Baprofeta bakolembeke te kokebisa bato ’te bakoki kotia mitema se na Nzambe, kasi Yudas alingi kotia motema na ba-Roma. Nzokande baprofeta bakolonga : bato ba Bokonzi bwa Nzambe bakoki kotia elikya na bato ba lokumu mpe ba nkita te. Mpe ya solo, mibu soki nkama ibale na nsima ba-Roma bakokoma banguna ba bango, mpe bakosukisa ekolo Israel.
10,15. Banda boombo bwa bango o Asur ba-Yuda bazalaki na mokonzi wa solo wa ekolo lisusu te. Banda Esdra na Neemia banganga bakonzi nde bakambi ekolo. Mpo Yonatan azala mokambi wa solo basengelaki kokomisa ye nganga mokonzi, nzokande bobele mwana wa nganga mokonzi akoki kokitana tata wa ye o mosala moye (Lv 8 ; Ebr 5,4). Yango nde esali ’te bato mingi, ná ba-Kasideo, baboyi ’te Yonatan akoma nganga mokonzi .
10,59. Bato ba Nzambe baye bakoti o makambo ma politiki bakoki kozanga bosembo te. Yango wana Yonatan akokoka kobongisa makambo ma ekolo te.
13,1. Simoni aluki kokabola bakonzi ba bikolo bisusu, kasi boboto azweli ekolo ya ye, ekosepelisa bato ba ye mi­ngi te. Mosika te ‘Simoni mobikisi’ akomi ‘Simoni monyokoli’ wa bato ba ye. Mibu soki 50 na nsima, bankoko ba ye bakozala banguna mpenza ba Yezu.


MAKABE 2

1,1. Ba-Yuda baye bakomeli bandeko o Ezipeti bauti kokweisa banyokoli ba bango ; basilisi kopetola Tempelo.
6,1. Ndenge nini Nzambe akoki kolingisa ’te bato ba ye bakonyokwama ? Nzambe nta ngolu alingi se kobokolo mpe kobongisa bango : soko balandi nzela ya masumu bakozwa bolamu bwa solo te.
9,1. Ata lisolo liye lya liwa lya mo­nyokoli Antioko likeseni moke na maye tokoki kotanga o 1 Mak 6, liteya lya lyango lizali se lyoko : malamu na mabe manso makofutama, ata ndele ! Mbala mingi bobele bokono na mpasi bikoki kobongisa makanisi ma bato babe : liboso bamiyebaka na lolendo te ; na nsima bakomi komono bozoba bwa lolendo ; mpasi bakomi koyoka esali ’te bakundola mpasi inso bayokisaki bato batau na bato bake.
12,38. Awa tokoki kotanga mbala ya liboso o Biblia ’te bato bamitungisi mpo ya maye makoya nsima ya liwa : bandeko ba biso soko bakufi na masumu o mitema, bakozwa nde bolamu boye Nzambe alakeli bato ba ye baye basalelaki ye na motema moko ? Yango wana Yudas atindi ’te basambela mpo ya bandeko bawei. Biso bato tozali naino na bomoi awa o nse tozali se lisanga lyoko na bato basili bawei. Tala mpe 2 Mak 15,12.
