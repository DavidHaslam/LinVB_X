KORINTI 2


1,1.	–Timoteo : Ntango Polo akomi o Efeze ayoki ’te bakristu ba Kori­nti bakomi na makambo ma kwokoso ; yango wana atindeli bango Timoteo mpo ’te azongisa boyokani o kati ya ba­ngo. –Akaya : nkombo esusu ya Greke.
1,8. Azia : Tala Rom 16,5.
1,13. Mpo ya kolakisa bango ’te maye akomi mazali se lokola maye ateyaki, Polo alimboleli bango lisusu makambo masusu.
1,14. Mokolo : Tala 1 Kor 3,13.
1,19. Silvano : Nkombo ya ye esusu ‘Silas’ (Bik 15,22 ; 18,5).
1,20. ‘Amen’ lokola ’te : Iyo, ezali se bongo !
1,23. « Nzambe ayebi motema mwa ngai » mazali maloba ma ndai, lokola ’te : « Soko nakosi, Nzambe akoki kopesa ngai etumbu ».
2,3. Monkanda moye Polo atangi awa mobungi.
2,5-10 : Atangi moto ona na nkombo te (tala mpe 7,12). Awa bapesaki ye etumbu mpo afingaki Polo (likambo lindimami na bakristu basusu ba Korinti), Polo asengi bango balimbisa ye.
2,13. Tito : azalaki mokristu abotamaki na baboti bapagano. Azalaki moko wa basalisi banene ba santu Polo (tala 7,6 ; 8,6 ; 12,18). Polo akomeli ye mpe monkanda.
3,3. ‘O mitema mya bato’ : Polo akomi : ‘O meza ya misuni’ ; na maloba maye apesi moulani mwa meza ya misuni na meza ya mabanga (Bob 24,12) ; akundoli awa mpe maloba tokoki kotanga o Ez 36,26.
3,7. Polo akomi ‘bosaleli liwa’.
3,13. Tala Bob 34,33-35.
4,4. Satana : Polo akomi : ‘nzambe wa mokili moye’ ; akobengaka ye mpe ‘nkumu wa mokili’ (Ef 2,2 ; 6,12 ; 1 Kor 2,6. Tala mpe Yo 12,31 ; 16,11).
4,6. Tala Lib 1,3 (Yo 8,12 ; Ef 1,18).
4,7. Polo akomi : ‘Tozali komeme elo­ko eye ya motuya o mbeki ya libazi (itau)’.
4,13. Tala Nz 116,10.
5,1. ‘Ema eye, ndako ya biso’ mpe ‘ndako esusu o likolo’ : Na ndakisa eye, mpe na eye ya ‘elamba’ Polo alakisi ’te nzoto ya moto eye ekopolo ekobongwana nzoto ya nkembo o mokolo mwa nsekwa (tala mpe 1 Tes 4,15).
5,3. To : ‘Toyebi te soko tokolata elamba to te, mokolo Mokonzi akoya’.
5,13. Polo akomaki liboso monkanda na maloba makasi, kasi asalaki bongo se mpo ya bolamu bwa bango : basala lokola Nzambe atindi.
5,16. Liboso ’te Kristu abyangaki ye o mosala mwa Eklezya, Polo ayebaki Kristu bobele na miso ma nzoto, lokola baninga ba ye ba-Farizeo banso bayebaki ye, kasi bayambi ye te. Yango wana azalaki konyokolo bakristu. Kasi sikawa ayebi Kristu lokola Mobikisi.
6,2. ‘Ntango ebongi’ (tala Iz 49,8) ezali eleko ut’o nsekwa ya Kristu kin’o mokolo Kristu akozonga o nsuka ya molóngó Tala mpe Rom 3,26.
6,13. Nsima ya molongo moye Polo akati lisolo liye, akobi lyango o 7,2.
6,15. Belial : nkombo yoko bakopesaka zabolo.
8,1. Biklezya bya Masedonia (Filipi na Tesaloniki) bapesaki Eklezya ya Korinti (Akaya) ndakisa mpenza ya bokongoli makabo.
8,7. Basusu babongoli boye : ‘Bolingi tokolingaka bino boye bozali kosangisa biso na bino’.
8,15. Ba-Ebrei bazalaki na manu ntongo inso : tala Bob 16,18.
8,18. Mbele ezali Luka, oyo akomi Nsango Elamu mpe Bikela bya Bapostolo.
10,4. Ekomami : ‘Kobuka matongi makasi’.
10,7. Basusu babongoli boye : Bino bozali katala makambo lokola mamononi na miso.
10,17. Tala Yer 9,22-23 ; 1 Kor 1,31. 
Polo aleki baye baweli ntembe na ye na mosala moye asalaki : ye moto aba­ndisaki Eklezya ya Korinti ; kasi alingi komikumisa na yango te, mpo Nzambe atindaki ya mosala mwango.
11,1. Polo asengeli komikumisa, mpo bafundi ye na makambo ndenge na ndenge (4.6.11), yango wana aku­ndoleli bango ntina nini asengeli kosakola mpe kosala mosala mwa apostolo wa Kristu.
11,13. Na bosangeli Nsango Elamu Polo aluki litomba lyoko te ; kasi bapostolo ba lokuta basangeli Nsango Elamu ya lokuta bazali na lofundo, mpe bakolukaka bolamu bwa bango moko.
11,24. Engebene na mobeko mya ba-Yuda bakokaki kobete moto fimbo bobele mbala ntuku inei ; mpo ’te balekisa yango te bazalaki kobete ye mbala ntuku isato na libwa.
12,2. 	–Nayebi moto moko wa Kristu : Polo azali komitanga o makambo mana lokola makwelaki moto mosusu, se na motema bosawa.
–O paradizo : Polo akomi o likolo lya masato, lokola ’te o esika Nzambe azali, engebene na makanisi ma bato ba kala.
13,1. Tala Mbk 19,15 : Mt 18,16 ;
1 Tim 5,19. Polo ayebisi ’te bobele baye basalaki mabe bakozwa etumbu.
13,7. Polo alobi ’te boyei bwa ye bokozala lokola etumba (13,1). Polo akobunda na nguya ya Kristu (13,3...). Soko ba-Korinti babongoli mitema te, Polo akolonga etumba (13,6-7). Soko babongoli mitema, Polo akolakisa bokasi bwa ye te (13,9) ; bongo bango bakolonga (13,7) mpe Polo akosepela (13,9).