1,1. Tala Mbk 34,5-9.
1,4. Na maloba maye balakisi Kanana. Ezalaki na ndelo mosika te lokola ekomami awa, kasi balingaki kolakisa ’te ezalaki ekolo enene mpenza. Epai esusu bakotangaka yango mpe ekolo ya ba-Iti, mpo ba-Iti mingi bafandaki mpe o Kanana.
1,8. ‘Buku ya Mobeko’ etangemi awa ezalaki na mitindo minso mikomami o buku ya Mbk (6,6 ; 29,2 ; 30,10 ; 31,26).
1,12. Moze apesaki mabota ma Ruben, Gad na Manase mabelé o ngambo ya Esti ya Yordane, kasi atindaki bango ’te basalisa bandeko ba bango mpo ya kobotolo bikolo o ngambo ya Westi ya Yordane.
2,5. Lokola bingumba mingi o ntango ena, Yeriko ezingami na efelo elai, eye eto­­ngami makasi. Ezalaki na bizibeli biye bazalaki kokanga na butu mpo ya komibatela na banguna.
2,15. Lininisa liye lizalaki o nsonge ya efelo ya engumba mpo ya konongo bisika bya penepene.
2,18. Bakangaki elamba eye mpo ya kolakisa ndako ya Rakab.
3,3. Sanduku eye esalemi na mabaya kitoko ekokundolela bato ba Israel ’te Nzambe azali o ntei bango. Bamemeki yango o boso bwa bato banso mpo ya kolakisa ’te Yawe moto azalaki kokamba bango ntango bakei kobotolo mokili mo­ye alakeli bango. Mpo ya komemya yango bato bazalaki kolanda yango mwa mosika.
4,1. O eteni eye basangisi masolo mabale. O lisolo lya liboso balobi ’te Yozue atelemisi mabanga 12 o kati ya Yordane mpo ya kokundola ’te Yawe akatisi ba­ngo ebale eye. Ezalaki naino wana o ntango lisolo liye likomami. O lisolo lya mabale bapanganaki mingi na ndako esantu ya Gilgal, eye ezalaki kati ya Yordane na Yeriko ; Yozue atiaki mabanga mana o Gilgal. O esika eye ba-Israel bazalaki koya mingi kokumbamela Nzambe, liboso ’te batonga Tempelo o Yeruzalem (1 Sam 11, 15 ; 13,4-15 ; 15,12-33).
5,2. Bokatisi nzoto : tala Lib 17,9-27 ; (Lv 3) na ndimbola.
5,9. Bayokaki nsoni o Ezipeti mpo bakatisi nzoto naino te, se lokola ba-Ezipeti.
5,12. Manu : tala Bob 16,4-36+.
6,1. O eteni eye mpe basangisi masolo mabale. Mpo ya kolakisa ’te Yawe moto alingisi bango kobotolo Yeriko, bayebisi ’te babeti mindule mpe batamboli na Sa­nduku zongazonga na engumba. Kasi o 6,10 totangi ’te Yozue apekisi bango kosala makelele, mpo alingaki kobotolo Yeriko na mayele.
6,17. Mpo ya kotondo Nzambe oyo alongisi bango, babomi bato mpe babebisi biloko binso bizalaki o engumba. O ntango ena bakanisaki ’te bakopesa bo­ngo lokumu na Nzambe ; bayebaki naino te ’te akoyoka nsomo soko babomi bato mpo ya kokumisa ye.
7,21. O Biblia bazalaki kolakisa Babilon mbala mingi na nkombo ‘Sinear’ (Lib 10,10 ; 12,2), molato kitoko mpenza, mpo Babilon ezalaki engumba ya lipombo.
7,24. Akan asali lisumu, kasi bakotu­mbola ye na libota lya ye mobimba, enge­bene na momeseno mwa kala, moye mokopekisama na nsima (Mbk 24,16).
7,26. Akor lokola ’te (lobwaku) ‘la likambo lya mpasi’.
8,12. O 8,3 bakomi 30 000. Mbele bazalaki 5000, lokola ekomami awa.
8,31. Tala Bob 20,25 ; Mbk 27,5-7.
8,34. Bakobenisa moto oyo atosi Mobeko mwa Nzambe ; oyo atosi mwango te bakotombela ye bobe.
9,1. Ata engumba ya bango ezalaki pene (km. soki 15), ba-Gabaon bakosi ’te bauti mosika mpo ’te babomama te. O ntango ya kala bazalaki kokata bondeko bobele na bingumba biye bizalaki mosika.
9,27. Esika yango ezali Yeruzalem, eye Salomo akotonga.
10,4. Bakonzi baye batano basilikeli ba-Gabaon mpo bakati bondeko na ba-Israel. Yango wana bingumba bikoyokana mpo ya kotumbola ba-Gabaon mpe kobengana ba-Israel.
10,10. Na lilonga liye Yozue akokoka kobotolo eteni ya Sudi ya ekolo. Na butu yoko apusani km. 30 banda Gilgal o esobe tee Gabaon o ngomba.
10,13. ‘Moto Semba’ ezali buku ya kala eyebani lelo lisusu te. Mbele ezalaki na masolo mingi ma eleko ba-Israel bazalaki o Kanana mpe ma bakonzi ba liboso. Buku eye etangemi mpe o 2 Sam 11,18.
10,16. Lisolo liye lizali lya sika ; tota­ngi lisolo lisusu (etumba o Gabaon) o 10,6-15.
11,1. Madon na bingumba bisusu bitangemi awa ezali o eteni ya Nordi ya ekolo. O eteni ya 10 totangi ndenge Yozue alongi bakonzi batano ; o eteni ya 11 tokotanga ndenge Yozue akolonga bakonzi ba Nordi bayokani mpo ya kobundisa ye.
11,12. Tala Mbk 7,2-6 ; 20,16-18.
13,8. Tala Mit 32 ; Mbk 3,12-17.
14,5. Tala Mit 34,16-28.
14,6. Tala Mit 13-14. Kaleb azalaki moto wa Israel te, kasi abundisi banguna elongo na ba-Israel.
15,1. Banda eteni eye balakisi ndelo ya mabelé ma libota lyoko lyoko, mpe nkombo ya bingumba bya bango.
15,63. Bayebisi mpe nkombo ya bingu­mba biye bakokaki kobotolo te. Toyebi bongo ’te ba-Israel bakokaki kobotolo ma­belé mpo ya kofanda wana bobele na mpasi mpe malembe, leka maye totangaki liboso o masolo ma Yozue oyo alongi ba-Kanana. Tala mpe Baz 1,19-35.
19,40. Bakabelaki libota lya Dan mabelé o Sudi-Westi, kasi bakokaki kofanda wana te ; bongo bakei koluka mabelé o Nordi (Baz 1,34 ; 18).
20,1. Engebene na mimeseno mya ba-Israel mpe mya bikolo bisusu, moto nyonso abomi moto mosusu, ata na nko te, asengeli kobomama na bandeko ba moto akufi (Bob 21,23-25). Kasi bamonoki noki ’te mobeko mwango mokokaki kotala moto te soko moto abomami na nko te. Yango wana baponi mboka isusu, esika baye babomaki moto na nko te bakokaki kokima mpo babika. Tala mpe Bob 21,13 ; Mt 35,9-34.
22,12. Ntango batongi altare esusu o Esti ya Yordane, bato ba mabota masusu basiliki. O miso ma bango ezalaki lokola bakinoli altare o Silo ; bazalaki mpe kobanga ’te yango ekoki kobebisa boyokani kati ya mabota manso. O eyano ya ba­ngo, Ruben, Gad na ndambo ya libota lya Manase bameki kokitisa bango mitema.
22,17. Tala Mit 25,1-27 ; Mbk 4,3.
23,1-16. Awa Yozue akundoli naino maye ba-Israel bakoki kobosana ata moke te : Yawe moto aponi Israel mpe akabeli bango mabelé mpo bafanda wana ; bongo bakoki komibebisa te na makambo ma bikeko ma ba-Kanana, mpe bandima mabala ma bana ba bango na ba-Kanana te. Soko batosi mibeko mya Yawe, akobenisa bango ; soki te, mabe makokwela bango. Tala Mbk 6,5 ; 7,1-6 ; 28,1-67 ; 32,30... .
24,1. Lokola ’te o boso bwa Sanduku ya Bondeko.
24,2. Efrate.
24,14. Yozue akundoleli bato ba Israel ’te bankoko babosanaki Nzambe wa solo mpe bakumbamelaki banzambe ba Ezipeti ntango bakangemaki bo baombo o Ezipeti. Moze azongisi bango o nzela ya solo.
24,32. Lokola Yozefu asengeki liboso ’te akufa (Lib 33,18-20).