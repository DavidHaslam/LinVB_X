MARKO


1,1. O molongo moye mwa liboso Marko alakisi ntina nini mpenza akomi Nsango Elamu : moto ayambi ’te Yezu azali Kristu, lokola ’te Mesiya to Mobikisi, akobika mpo ayambi Nsango Elamu ya ye.
1,15. Yezu ateyi ’te Nzambe nta ngo­lu akobyangaka bato banso babo­ngola mitema, mpe bandima ye bo mokonzi wa bango. Esengo na bato bakoyamba nsango eye !
1,21. Sinagoga : ndako ya losambo ya ba-Yuda.
1,24. Na nkombo eye zabolo alakisi ’te Yezu azali Mesiya.
1,25. Yezu apekisi bazabolo baloba te ’te azali Mesiya (tala mpe 1,34 ; 3,12) ; se bongo apekisi bato baye abikisi (1,44 ; 5,43 ; 8,26) na mpe bapostolo ba ye (8,30 ; 9,9). Bakoki koloba yango bobele nsima ya liwa lya ye (15,39), mpo ba-Yuda mingi bazalaki kozila Mesiya bo mobikisi wa bango o maboko ma ba-Roma. Biso mpe tokoki koyamba Kristu bobele mpo ya maka­mwisi ma ye te, kasi mpo ya liwa mpe nsekwa ya ye.
1,40. O miso ma ba-Yuda bokono bwa maba bokomisi moto mbindo.
2,1. Yezu atikaki Nazarete, akomi kofanda o Kafarnaum (tala mpe Mt 4, 13).
2,5. Awa Yezu alimbisi ye masumu, alakisi ’te ayei liboso mpenza kobikisa molimo mwa ye.
2,13. Etima ya Tiberia to ya Galilea. 
2,14. Mateo.
2,17. Basemba : baye bazali komimono bato balamu mpenza, lokola ba-­Farizeo ; baye batangemi basumuki bazalaki kotosa mibeko minso te.
2,20. Mbele Yezu alingi koyebisa ’te liwa lya ye lizalaki lisusu mosika te.
2,26. Abiatar : Tala 1 Sam 21,1-6. Nko­mbo ya nganga Nzambe ona ezali Akimelek. Nzokande mwana wa ye Abiatar ayebani mingi, leka tata wa ye.
2,28. Yezu alobi polele ’te azali na likoki lya Nzambe mpo ya kolimbola ndenge nini basengeli kosantisa mokolo mwa sabato. Tala Mt 8,20.
3,6. Bato ba Erode : ba-Yuda baye bazalaki kondima Erode na politiki ya boyokani na ba-Roma. Ba-Farizeo balingi koyokana na bango, mpo bayebi ’te bakoki kosala Yezu eloko te soko Erode andimi te.
3,31. Tala Mt 12,46. Tolinga bandeko, solo, kasi baye banso bameki kosala maye Nzambe alingi bakomikomisaka bandeko ba Yezu.
4,12. Soko moto aboyi koyamba Nzambe akoyoka lisusu mateya ma Nzambe polele te ; bongo moto oyo akozwa lisusu ngrasya te (tala Iz 6,9-10).
4,13. Bapostolo batongoli maloba mpe bikela bya Yezu te : Marko azo­ngeli likambo liye mbala mingi (6,52 ; 7,18.21.33 ; 9,10.32 ; 10,38).
4,27. Ba-Yuda mingi bakanisaki ’te Bokonzi bwa Nzambe bokoya na mbalakaka, bongo bato banso baka­mwa. Kasi Yezu alimboli ’te Bokonzi bwa Nzambe bokoya malembe malembe, se lokola mboto ekokolaka.
5,2. Bikunda biye bizalaki o kati ya ngomba ya mabanga, to o mabulu o mabelé.
5,33. Bokono boye boyokisi mwasi nsoni, kasi bokomisi ye mpe moto mbindo o miso ma bato.
5,37. Bobele bayekoli baye bazalaki elongo na Yezu mokolo abongwani na nzoto (9,2), mpe mokolo ayoki mpasi o Getsemani (14,33).
5,39. Mwana oyo akufaki solo, kasi Yezu alingi koloba ’te akangemi na liwa te, mpo akosekwa. Nsekwa eye ezali elembo ya nsekwa ya biso : mpo ya boyambi toyebi ’te liwa lya biso liboso lya nsekwa likozala se lokola mpongi. 
5,41. O lokota la ba-Aram, lokota Yezu azalaki koloba.
6,5. Yezu aboyi kosala makamwisi o miso ma bato bazangi boyambi. Akobikisa bobele baye bazali na boyambi.
6,15. Ba-Yuda bazalaki kozila Elia lokola profeta oyo asengeli koya kobongisela Mesiya nzela.
6,37. Denario : lifuta lya mokolo moko.
7,5. Mimeseno miye mya bankoko mikomami o Bikoma Bisantu te, mizalaki se mibeko mya bato. Marko ali­mboli myango, mpo akomi Nsango Elamu mpo ya bakristu baye bazalaki ba-Yuda te.
7,11. Biloko moto alakelaki Nzambe mpo ya Tempelo, bikokaki kopesama lisusu te mpo ya kosalisa bato.
7,27. Bana : Ezali ba-Yuda baponomi bazala ekolo ya Bokonzi bwa Nzambe. Ata Yezu atindami liboso mpenza epai ya bango, aboyi mpe bato basusu (bapagano) te ; eyano apesi mwasi oyo elakisi yango malamu.
8,10. Mboka eye eyebani te ; mbele euti o lokota la ba-Aram mpe Marko akomi yango malamu te.
8,11. Balingaki komono likamwisi linene, bandima bongo ’te Yezu azali Mesiya wa solo.
8,15. Na nkisi ya mampa Yezu alakisi lolendo mpe bokibisi bwa ba-Farizeo mpe bwa bato ba Erode. Tala mpe Mt 16,6.
8,21. Yezu akebisi bapostolo ’te bapangana bobele na mambi ma mokili moye te ; bakanisa mpe ntina ya boyei bwa Kristu na makamwisi ma ye.
8,22. Izaya (35,5) asakolaka ’te bakoyeba Mesiya na bobikisi bato ba lola­nda.
8,25. Moto wa lolanda abiki na mbala yoko te, lokola moto akoki kozwa mwinda mwa bosolo na mbala yoko te.
8,29. ‘Kristu’ (o lokota la ba-Greki), ‘Meshiah’ (Mesiya) o ebrei : moto oyo bapakoli mafuta masantu, mpo ’te akoma mokonzi wa ekolo to nganga Nzambe (o Bondeko bwa Kala). Bazalaki kotala baprofeta mpe lokola ‘bapakolami’ mpo bazalaki na Elimo ya Nzambe. Yango wana Petro alobi ’te Yezu atondi na Elimo Santu, Moponomi wa Nzambe, profeta monene mpe Mobikisi oyo bato batiaki mitema.
8,38. Bato babóyá Nzambe : o Bikoma Bisantu bakomi ‘bato ba bokali’. Na maloba mana baprofeta bazalaka kopalela ba-Yuda, mpo bazalaki kokumisa banzambe ba lokuta, lokola moto wa libala aluki basi basusu.
9,1. Na nguya enene : Maloba maye mazali polele mpenza te. Mbele Yezu alingi koloba ’te ye moko azalaki koteya bobele mwa liboke lya bato, kasi nsima ya nsekwa ya ye bato mingi bakoyamba Bokonzi bwa Nzambe mpo ya mateya ma bapostolo. Nzokande nguya enso ya Bokonzi bwa Nzambe bokomonono bobele o nsuka ya molóngó.
9,2. Ngomba Tabor. Tala mpe Mk 
5,37.
9,13. Yezu alobi bongo mpo ya Yoane Mobatisi.
9,29. Bazabolo baleki bato na bokasi. Tokoki kolonga bango bobele na nguya ya Nzambe.
9,44+46 = 49,48.
10,24. Ba-Yuda bakanisaki ’te nkita ezali elembo ya bobenisi bwa Nzambe.
10,38. Kokula batisimo : awa lokola ’te : koyoka mpasi.
10,51. Moto wa lolanda abengi Yezu awa ‘Rabuni’ eleki nkombo ‘Rabi’ (moteyi) na lokumu.
11,9. « Ozana » : tala Mt 21,9+. 
Tala Nz 118,25-26.
11,14. Marko alimboli likambo liye lya nzete ya figi te. Mbele Yezu asili ayebisi awa na bobombami likambo likokwela Yeruzalem, engumba eye eboyaki kobota mbuma ya bosembo mpe ya boboto.
11,26. (Kasi soko bino bolimbisi te, Tata wa bino o likolo akolimbisa mabe ma bino mpe te). Tala Mt 21,33 ; Lk 20,13.
12,14. Kaizaro : Bakonzi banene ba Roma bazalaki komipesa nkombo eye ya lokumu.
12,37. Na motuna moye Yezu awa­ngani te ’te azali nkoko wa Davidi, kasi alingi ’te bato bayeba ’te auti na Oyo aleki Davidi na bonene. Ntina yango Davidi akokaki kobenga Mesiya oyo akoya ‘Mokonzi wa ye’.
13,2. Yango esalemi o mobu mwa 70, ntango ba-Roma babuki Tempelo mobimba mpo ya botomboki bwa ba-­Yuda.
13,30. O eteni ya yambo (13,14-23) Yezu alobaki likambo lya nsuka ya Yeruzalem, o eteni ya mibale (13,24-27) likambo lya nsuka ya molóngó. Mbele o eteni ya misato (13,28-32) Marko asangisi makambo maye mabale. 
14,25. Vino ya sika : nsima ya nsekwa ya ye, Yezu akoyambama o Bokonzi bwa nkembo ya Nzambe. Yezu alakisi Bokonzi boye lokola limpati.
14,52. Mbele ezalaki Marko ye moko. 
14,61. Na maloba maye atangi nkombo ya Nzambe, zambi ba-Yuda baba­ngi kotanga nkombo ya yemei : ‘Yawe’.
14,62. Lokola profeta Daniel (7,13) asakoli. Tala mpe Nz 110,1.
15,1. Pilato : tala Mt 27,2+.
15,21. Alesandro na Rufo : bakristu ba Roma bayebaki bango (Rom 16,13). Toyebi ’te Marko akomi mateya ma santu Petro, mpe Petro awei o engumba Roma.
15,28. (Bakokisi bongo maye makomami o 53,12 : Bakotisi ye o molongo mwa bayibi.)
15,38. Yango elakisi 'te eleko ya mabonza na milulu mya 
Bondeko bwa Kala esili. Na liwa lya ye o kuruse, Yezu abandi eleko ya 
Bondeko bwa Sika : abonzi libonza lya seko se lyoko mpo ya libiki lya bato banso.
15,42. Mokolo mwa bolengeli : Mokolo mwa mitano bazalaki kolengele bilei bya mokolo mwa sabato.
16,12. Bayekoli ba Emaus : tala Lk 24,13-35.