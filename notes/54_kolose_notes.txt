KOLOSE


1,2+4. Bandeko : Ekomami ‘basantu’ (1 Kor 1,2).
1,5. Awa mpe Polo atangi mabongi manene ma bakristu, boyambi, bolikyi na bolingi, molongo moko (tala mpe
1 Kor 13,13 ; 1 Tes 1,3).
1,7. Epafrase : Moto wa yambo oyo asangelaki ba-Kolose Nsango Elamu (tala Kol 4,12-13 ; Flm 23).
1,15. Tokoki komono Nzambe te. Kasi, awa Nzambe Mwana akomi moto, baye bamoni Yezu bamoni nkembo ya Bonzambe o elongi ya ye.
1,16. Ba-Yuda basusu bazalaki kotanga mangomba ma banzelu na nkombo iye (Ef 1,21) ; Polo alobi ’te Kristu aleki banzelu na bilimo banso.
1,18. O Eklezya bakristu banso bazali lisanga na Kristu, mpo ya boyambi mpe mpo ya batisimo ya bango. Polo alakisi yango na elili eye ya Nzoto (Eklezya) na Motó (Kristu).
1,26. Maye Nzambe alakelaki liboso bobele na ba-Yuda, makotala bato ba bikolo binso banda boyei bwa Kristu (Ef 3,3-12).
2,1. Laodisea : Engumba penepene na Kolose, o Azia.
2,8. Polo alobi bongo mpo ya mateya ma lokuta bazalaki koteya o Kolose.
2,11. Ba-Yuda bakokataka nzoto, bakristu bakokula batisimo. Na batisimo tobiki mobimba, tosangani na Kristu oyo akufi mpe asekwi.
2,14. Monkanda mwa nyongo : Mobeko mwa ba-Yuda na mitindo mya mwango minso : awa Kristu awei o kuruse, mozali lisusu na ntina te.
2,16. Mimeseno ba-Yuda basusu baye bakomaki bakristu balingaki kopesa bo mobeko na bakristu ba Kolose.
2,20. Mpo ya boyambi mpe batisimo, mokristu akoki kokangema na mabe ma mokili te (akufi elongo na Kristu) ; akomi na bomoi bwa sika bwa mwana wa Nzambe (asekwi elongo na ye).
3,18. Mokristu asengeli kotosa mibeko mya bolingani mpe mya bome­myi, miye Polo atangi awa, mpo ya Kristu mpe na nguya ya ye. Tobosana te ’te bazalaki komemya baombo o ntango ena te, lokola Polo asengi yango. Tala mpe Flm.
3,24. Lisango lya nkembo o likolo : 
Tala Ef 1,14.
4,7. Tisiko : moninga mosusu wa mosala wa Polo (Bik 20,4 ; Ef 6,21 ;
2 Tim 4,12 ; Tit 3,12).
4,10. Aristarko (Bik 20,4 ; 27,2) na Marko, oyo azalaki moyekoli wa Petro (Bik 12,25 ; 15,37-39 ; 2 Tim 4,11), bazalaki kosala na boyokani na Polo.
4,13. Bakoki mpe kobongola boye : akosalaka makasi...
4,14. Luka : Mokomi wa Nsango Elamu yoko mpe wa Bikela.
4,16.	Mbele monkanda ona bakristu ba Efeze.
Basengelaki kotanga minkanda mya santu Polo o masanga ma bandeko banso (tala 1 Tes 5,27), na nsima balekisa mwango epai ya bakristu ba mboka ya penepene (2 Kor 1,1).