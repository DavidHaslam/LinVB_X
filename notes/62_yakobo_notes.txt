YAKOBO


1,1. Mabota zomi na mabale mapala­ngani o mokili : Ba-Yuda baike bakomi bakristu. Mingi o kati ya bango bafandi o Palestina te, kasi bapalangani o bikolo bisusu, o kati ya bapagano. Bakomaki o bikolo bisusu ut’o ntango ya boombo (Ydt 5,19 ; Nz 147,2 ; Yo 7,35). O nkota isusu bakotangaka bango ‘diaspora’.
1,5. Ezali bwanya bwa mokili te, kasi ‘koyeba ezalela nini ekosepelisa Nzambe’.
1,9. Tala mpe 2,5. O Eklezya babola bazali bato ba lokumu mpenza, se lokola Kristu alingi (Mt 5,3). O Bondeko bwa Kala baprofeta basakolaka se bongo. Bato ba nkita bakoki mpe kozwa lokumu, kasi basengeli komikitisa liboso.
1,17. O Lib 1,14-18 mpe o Nz 8,4 totangi ’te mwese, sanza na minzoto minso bikelami na Nzambe. Bapagano baike bazalaki kopesa byango lokumu lokoki se na Nzambe. Nzokande bobele Nzambe akobongwanaka te, akozimaka mpe te.
1,25. Sam. Yakobo atangi awa Nsango Elamu eye ezali kokamba bakristu ‘Mobeko mopusi bolamu’. Ya solo, bo­lingi Nzambe na mpe baninga bozali lokola mama wa mibeko minso (2,8-13 ; Mt 7,12). Mobeko moye mobomi Mobeko mwa Moze te, kasi mokokisi mwango (Mt 5,17). Mobeko mwa sika mokozwela bato bonsomi, mokobikisa bango o mi­nyoko mpe o matungisi mautaki na Mobeko mwa Moze, na mateya ma ba-Farizeo mpe na masumu.
2,7. Nkombo kitoko eye : ezali nkombo ya Yezu Kristu, eye ekotangema o ntango ya batisimo. Yango wana baye banso bakuli batisimo bazwi nkombo ’te ‘bakristu’.
2,14. S. Yakobo (2,24) na S. Polo (Rom 3,28 ; Gal 2,16) balimboli moto na moto likambo lya ye, ata batangi se liloba lyoko ‘misala’.
S. Polo atangi misala miye miuti na boyambi Yezu Kristu te, kasi na botosi Mobeko mwa Moze. Mibeko na mi­meseno miye miuti na Nzambe te, kasi se na mayele ma bato, bakristu basengeli kotosa myango te.
S. Yakobo alendisi bakristu ’te basala misala mya boyambi (Rom 2,6 ; 6,17 ; 2 Kor 5,10 ; Gal 6,7.10), yango wana atangi liboso mpenza mobeko mwa bolingi Nzambe na mpe baninga. Boyambi botindi mokristu abota mbuma, asala misala milamu (Gal 5,6), akokisa bongo mobeko mwa Kristu.
2,19. Awa bazabolo bakoyambaka, bakolandaka se mayele ma bango moko. ‘Boyambi’ bwa bango bozali se koyeba, kasi kondima na motema te. Bazwi boyambi te lokola likabo liuti na Nzambe, liye Nzambe apesi bato bayambi ye mpo ’te babika.
3,1. Bakristu basusu balingi kokoma bateyi mpo ya bolamu bwa lisanga te, kasi mpo ya lokumu la bango moko (Mt 23,8).
3,10. Likambo lya kobenisa mpe ya kotombela bato bobe likotangemaka mbala mingi o Bondeko bwa Kala. Ndakisa : Lib 12,3 ; 27,29 ; Mit 23,11 ; 24,9 ; Yoz 8,34. Kasi mokristu akoki kotombela bato bobe te ; tala Lk 6,28 ; Rom 12,14 ; 1 Pe 3,9.
4,4. Mwasi aboyi mobali to mwasi wa bokali : Na maloba maye baprofeta bata­ngaka bato bazalaki koboya kotosa Nzambe mpe kokumbamela bikeko bya banza­mbe ba lokuta (Oz 2 ; Ez 16 ; Iz 57,3el).
4,5. Toyebi malamu mpenza te soko Yakobo alakisi awa maloba ma Lib 2,7 ; 6,3 ; Ez 36,27.
Zuwa lya Elimo lizali lokola zuwa mobali wa libala akoyokelaka mwasi wa ye : akoki kondima te ’te alanda mobali mosusu. Se bongo, Elimo akoki bobele kondima ’te totia mitema na Nzambe.
4,11. Moto akinoli, atongi mpe akitisi moninga, atali Mobeko mpamba, mpo atosi mwango te.
5,2. Nkusu : Tala Mt 6,19.
5,4. Mokonzi wa Mampinga (Sabaot) : Bapesaka Nzambe nkombo eye o Bondeko bwa Kala (Iz 5,9).
Moto aumisi kofuta mosali, akotisi ye o mpasi. Yango wana mobeko motindi nkolo afuta mosali wa ye mokolo na mokolo.
5,5. Mbele Mokolo mwa likambo : Mokonzi Nzambe akosambisa bato ba nkita mpo ya minyoko bayokisi basemba (Yer 12,3).
5,5-6. Koboma nyama mafuta mpo ya limpati lya eyenga elakisi : Esika bazali kopono bato ba nkita, koboya babola mpe konyokolo basemba (Nz 44, 23 ; Bw 2,10-20 ; Yer 12,1-2).
5,14. Bakolo ba Eklezya : Baye bakoyangelaka Eklezya lisanga.
5,14-15. Ezali sakramento ya Mafuta ma bakoni. O likita lya Konsilio ya Trente bandimaki yango.