YOANE


1,1. Yoane atangi Yezu Liloba Nzambe atindeli biso (1-5) : Ye Nzambe Mwana azalaki o libandela o mboka Nzambe. Elongo na Tata mpe na Elimo Santu akeli likolo na nse, apesi bato bomoi (9-14) ; na nsima akomi moto.
1,3. O Minkanda Misantu (Lib 1,6) ekomami : Nzambe alobi Liloba, mpe mwinda mobimi... likolo likelami ; binso bisalema se na Liloba lya Nzambe.
1,5. Molili : Ezali bato bayebi Nzambe te. Na Minkanda Misantu mya kala Nzambe amiyebisi na bato, kasi amiyebisi polele te. Awa atindi Mwana, amiyebisi polele, lokola mwinda moke­ngenge o kati ya molili.
1,7. Yoane Mobatisi alobi polele ’te azali bobele nzeneneke ; bongo bayekoli babete ntembe te : bobele Kristu azali Mwinda.
1,21. –Ba-Yuda bazalaki kozila Elia abongisela Mesiya nzela (Ml 3,23).
–Profeta asengeli koya : Mpo ya maloba ma Mbk 18,15 ba-Yuda bazalaki kozila profeta monene oyo akoya o ele­ko ya nsuka, mpe akosala makamwisi manene lokola Moze (tala Mt 16, 14).
1,29. Yoane akobengaka Yezu ‘Mwa Mpata’. Akanisi makambo ma Bondeko bwa Kala : Ba-Israel babiki o boombo bwa Ezipeti mpo ya makila ma mpata. Biso tokobika o boombo bwa masumu mpo ya makila ma Yezu.
1,31. Yoane Mobatisi ayebaki Yezu bo Mesiya te tee mokolo Yezu ayei kokula batisimo.
1,33. Kobatisa na Elimo ezali se mosala mwa Mesiya.
1,40. Moyekoli mosusu : mbele ezalaki Yoane yemei, mpo abosani ata ngonga ya bokutani bwa yambo na Yezu te.
1,45. Natanael : Mbele ezali nkombo ya ibale ya Bartelemeo (tala Mt 10,3).
1,49. Mbele na maloba maye Natanael alakisi te ’te Yezu azali Mesiya, zambi atangi ye mokonzi wa Israel.
1,51. Likolo likofungwama : elakisi ’te boyokani kati ya Nzambe na bato, boye bobebaki mpo ya masumu ma bato, bobongi lisusu mpo ya Yezu. Tala limoni lya Yakob (Lib 28,12).
2,1. Kana ezali penepene na Nazarete.
2,4. –Olingi nasala nini ? : ba-Ebrei bameseni mingi na maloba maye (tala Baz 11,12 ; 2 Sam 16,10 ; 19,23 ; Mt 8,29 ; Mk 1,24 ; 5,7 ; Lk 4,34 ; 8,28). Bayanolaki bongo soko bamoni ’te ntina ya kondima likambo bazali kosenge ezali te. Yango wana Yezu azongiseli mama wa ye : « Elaka ya ngai ekoki naino te. » Ata Maria azali mama wa Yezu, akoki kotinda ye mosala mwa Mesiya te.
–Elaka : ezali elaka eye Yezu akolo­nga zabolo o kuruse. Lelo Yezu abo­ngoli mai vino lokola elembo ya esengo ekozala mokolo elaka ekokoka.
2,17. Maloba maye (makomami o Minkanda Misantu) : Tala Nz 69,10.
2,21. Esika esantu : Ba-Yuda bakanisi Tempelo, kasi Yezu alobi bongo mpo ya nzoto ya ye. Mbala mingi Yezu alobi maloba maye ; baye bayoki mango, bayebi ntina ya maloba mana malamu te. Soko bazali motema malamu, Yezu akolimbolela bango (3,4 ; 4,15). Soko bazali motema mabe, akolimbolela bango te (6,60 ; 7,35).
2,23. Bayambi ye mpe bayambi ’te azali Mobikisi, lokola nkombo ya ye elakisi.
3,1. Nikodemo, moto wa Likita linene, azalaki kobanga ba-Yuda, kasi na nsima akokoma na mpiko (7,50 ; 19, 39).
3,3. Abotami mbala ya ibale = abotami lisusu. Yezu alingi kolakisa Nikodemo ’te batisimo ezali ebandela ya bomoi bokouta o likolo, kasi atongoli naino ntina ya maloba maye te.
3,8. Okomono mompepe te, okoyoka lokito, okomono bokasi bwa mompepe. Se bongo okomono Elimo te, kasi okomono mosala akosalaka o mitema mya bato.
3,14-15 : Tala Za 12,10. Ba-Yuda, soko nyoka eswaki bango, babikaki na botali nyoka ya mbengi eye Moze atulaki. Se bongo baye bakotala na boyambi Yezu o kuruse bakozwa bomoi bwa lobiko.
3,29. O Bondeko bwa Kala, libala mpe limpati lya libala mazalaki bilembo bya eleko ya boyei bwa Mesiya (Oz 1,2). O Bondeko bwa Sika ‘mobali wa bolo­ngani’ elakisi Mesiya (Mt 22,1-4 ; 25,1-13 ; Ef 5,25-33 ; Boy 19,7 ; 21,2).
4,3. Yezu alongwi o Yudea mpo ya zuwa lya ba-Farizeo : noki bakoluka koboma ye, kasi elaka ya liwa lya ye ekoki naino te.
4,9. Nsima ya boombo o Babilon, ba-Yuda balingaki lisusu te ’te ba-­Samaria bakumisa Nzambe o Tempelo.
4,10. Mai ma Bomoi : O Bondeko bwa Kala ba-Yuda bazalaki kotala mai lokola elembo ya libiki liuti na Nzambe, liye Mesiya akoyela bato (Iz 12,3 ; 55,1 ; Yer 2,13).
4,14. Tala 2,20. Mwasi ayoki ntina ya maloba ma Yezu te, kasi azali moto wa motema malamu ; yango wana Yezu alimboleli ye ntina : Yezu akopesa mai lokola ngrasya ya boyambi ekozwisa biso bomoi bwa seko.
4,20. Ba-Samaria batongaki Tempelo ya bango o ngomba Garizim penepene na Sikar. Ba-Yuda bato babomaki yango mibu soki 150 liboso lya Y.K., nzokande ba-Samaria bakobaki kokumisa Nzambe kuna.
4,22.	–Ba-Samaria : mbakisa ya biso.
–Awa Yezu alongisi ba-Yuda. Mesiya akobima epai ya ba-Yuda ; bongo mateya ma Nzambe ma solo mazali epai ya ba-Yuda.
4,36. Moto aloni : Yoane ; moto abuki mbuma : Yezu.
4,45. Eyenga ena : Pasika.
4,46. Awa Yezu akomi o Galilea, akei o Nazarete te, kasi aleki o Kana. Kuna bayambi ye malamu.
4,48. Awa Yezu alobi na ba-Yuda banso.
4,54. Likamwisi lya yambo : o mboka Kana (tala 2,11).
5,1. Yoane atangi nkombo ya eyenga te lokola ameseni. Tokanisi ’te awa alakisi eyenga ya Pentekoste.
5,14. Yezu alobi te ’te akomaki na mpasi ya nzoto mpo ya masumu (tala 9,3), kasi ayebisi ye ’te asengeli kobongola motema, soki te bokono bwa ye bwa molimo, boye boleki bokono bwa nzoto na bobe, bokosila te. 
5,17. Yezu asengeli kolanda mobeko mwa sabato te, zambi azali Mwana wa Tata : Tata mpe asengeli kotosa mobeko mona te.
5,19. Yezu awangani te, alobi ’te akokani na Tata, azali na nguya ya Ye.
5,20. Misala mileki... : kosekwisa bato mpe kosambisa bato : misala mya Nzambemei.
5,25. Ntango esili ekoki : Elaka ya nse­kwa ebandi esika Nzambe Mwana akomi moto ; ezali ebandela ya libiki.
5,31. Maye Yezu azali kosakola mpo ya ye moko, ba-Yuda bakoki kondima mango te (tala Mbk 17,6 ; 19,5). Yango wana Yezu atangeli bango Yoane Mobatisi, makamwisi ye moko akosalaka, na mpe maloba ma Minkanda Misantu.
5,32. Mosusu : Tata oyo atindi misala minene ngai nakosala (5,36) ; ye mpe alobi o Minkanda Misantu mpo ya ngai (5,39).
6,4. Na botangi Pasika Yoane asili ayebisi (na bobombami) ’te mampa maye Yezu aleisi bato ebele mazali elembo ya Pasika ya sika, ntango Yezu akomipesa bo Mampa ma bomoi bwa seko o Ukaristya.
6,7. Denario ezalaki lifuta lya mokolo moko. 
6,14. Profeta oyo : tala Yo 1,21.
6,15. Bato balingi kolubola Yezu mokonzi wa ekolo : ezalaki te mpo ya kokamba bango o mboka Nzambe, kasi mpo ya kobengana ba-Roma o ekolo ya bango. Kasi Yezu azalaki na makanisi maye ata moke te, yango wana akimi.
6,27. Yezu alobaki na mwasi wa Samaria bobele bongo, mpo ya mai (tala 4,13). Bobele Nzambe akoki kopesa bomoi bwa lobiko. Yezu mpe akoki kopesa bwango, zambi azali Mwana wa Tata ; Tata atii ye elembo.
6,30. Ba-Yuda basengi likamwisi lisusu ; likamwisi lya boyikinyi mampa likoki te, mpo batalaki mango lokola mampa mauti o likolo te. 
6,34. Ba-Yuda bakanisi se mampa lokola manu. Balingi koyamba naino te ; yango wana Yezu akolimbolela bango naino. Tala 2,19.
6,35. –Manu mazalaki kolendisa bobele bomoi bwa nzoto, mazalaki mampa ma likolo mpenza te (tala 32).
–Sikawa Nzambe akopesa mampa ma solo makopesa bomoi bwa seko. Mampa mango nde Yezu ye moko. Ko­yamba Yezu auti o likolo ezali lokola kolia bilei biuti o likolo.
6,46. Na maloba maye Yezu azalaki komilakisa.
6,51. Yezu akomikaba mpo ’te bato bazwa bomoi bwa Nzambe. Asali yango awa andimi kokufa o kuruse mpe na bomipesi bo bilei o Ukaristya.
6,62. Sikawa nazali o ntei ya bino mpe bokoki koyamba te ’te nazali na likoki lya kopesa bino nsuni na makila ma ngai. Boniboni bokoyamba ntango nakozonga o likolo mpe nakozala awa lisusu te ?
6,63. Na mayele ma ye moko, moto (nsuni) akoki koyeba ntina ya maloba maye te. Yango wana tokoluka ndimbola ya maloba mana epai ya Ekle­zya, zambi Elimo akolobaka na biso na monoko mwa Eklezya.
6,69. Mosantu wa Nzambe : Oyo Nzambe atindi ; o makambo manso akosalaka maye Ye alingi, mpo azali na Ye motema moko.
7,2. O nsuka ya sanza ya libwa, ba-Yuda bazalaki kofanda o ndako ya nkasa mpo ya kokanisa bobimi o Ezipeti, ntango bankoko bafandaka o eliki (Mbk 16,13). O eyenga ena bazalaki kotondo Nzambe na ntina ena mpe mpo ya mbuma babukaki o bilanga.
7,3. Bandeko ba ye : tala Mt 12,46+.
7,8. Yezu akoki koyamba lokumu bato balingi kopesa ye, bobele o ntango asili kolimbolela bango ntina mpenza ya boyei bwa ye (tala 12,12-19).
7,13. Na ‘ba-Yuda’ Yoane alakisi mbala mingi bato banso te, kasi bobele banganga bakonzi, ba-Farizeo na bateyi ba Mobeko. Nzokande o Yo 7,15 na 7,35 emononi polele ’te alakisi bato banso.
7,15. Ayekoli mateya ma bateyi ba Mobeko te.
7,21. Tee sikawa Yezu abikisi naino moto moko te, tika se moto akufa makolo o liziba lya Betzata ; ezalaki o mokolo mwa sabato (5,8).
7,23. Ba-Yuda basengelaki kokatisa mwana noki, balekisa mikolo mwambi te. Bongo soko nsima ya mikolo nsambo bakatisi mwana naino te, base­ngelaki kokatisa ye o mokolo mwa mwambe, ata ezalaki mokolo mwa sabato.
7,27. Ba-Yuda bayebaki ’te Mesiya akozala Mwana wa Davidi (tala 7,42 mpe Mt 2,4-6). Nzokande bakanisaki ’te Nzambe akobomba ye o miso ma bato banso tee mokolo akobimisa ye polele mpe na mbalakaka, esika akomilakisa bo Mesiya. Bayebaki mpe ’te Mesiya akobotama o Beteleme (tala Yo 7,42 ; 2 Sam 7,12 ; Mi 5,1), kasi bato mingi bayebaki te ’te Yezu abotami kuna ; bakanisaki ’te abotami o Nazarete, esika akolaki.
7,28. Bakomi : ‘Oyo atindi ngai akobukaka lokuta te, akokosaka te : mpo elakelaka ye kala, akokisi yango.’
7,38. Kanisa maloba Yezu alobaki na mwasi wa Samaria (4,10).
7,40. Profeta : oyo asengeli koya liboso lya Mobikisi.
7,52. Profeta : oyo asengeli koya o eleko ya nsuka (tala 1,21).
8,6. –Soko Yezu andimaki ’te ba­bwakela mwasi ona mabanga, mbele bato bamonoki yango mabe ; kasi ba-Farizeo baluki kofunda Yezu ’te akobuka mobeko soko akotika ye.
–Na bokomi na mosapi o mabelé, Yezu alakisi ’te ye moto abimisi likambo liye te.
8,11. Yezu alobi te ’te moto oyo akosala lisumu akosala malamu ; kasi akolikya ’te mosumuki akobongola motema.
8,12. Mwinda : elembo ya libiki Mesiya akoyela bato (Iz 8,22-9,1 ; Lk 1, 79).
8,24. Ngai Nazali : Na maloba maye Yezu amipesi nkombo ya Nzambemei (Bob 3,14). Ntina ya nkombo eye : Ngai nazali Mobikisi wa Israel, Nzambe mpenza (Boy 1,8).
8,28. Mokolo mwa liwa lya ye o kuruse. Kasi Nzambe akotombola ye, mokolo akokamata ye epai ya Ye o Bokonzi bwa Ye bwa nkembo. Mokolo mwango Yezu akomonono na ndenge ya ye ya solo.
8,31. Babandaki koyamba ye, kasi bakomi naino mpenza bayekoli ba ye te ; yango wana Yezu alobi na bango ’te bazali bana ba solo ba Abarama te (8,39), mpo balandi ndakisa ya ye ya boyambi mpe ya misala te.
8,32. Yezu akoyela bato bosolo boye o nkombo ya Tata wa ye. Ba-Yuda bazalaki kokanisa se bonsomi bwa ekolo ya bango o maboko ma ba-Roma ; yango wana bayambi te.
8,41. –Tata wa bino zabolo (tala 8,44).
–Bana ba bokali : koboya Nzambe elakisami o Bondeko bwa Kala (Ez 16 ; Oz 1-2) lokola mobali asali bokali.
8,48. O miso ma ba-Yuda ba-Samaria balekaki bapagano na bobe, yango wana liloba ‘mo-Samaria’ lizalaki lifinga linene.
8,56.–Yezu alobi ’te ye moto akokisi elako eye Nzambe alakelaka Abarama.
–Amonoki ngai : Esika Izaka abotami, Abarama amoni Yezu na miso ma boyambi (Ebr 11,13).
8,58. Awa Yezu amipesi nkombo Ngai Nazali, ayebisi ’te azali na nsuka te ; azali komitia molongo moko na Nzambe, yango wana ba-Yuda balobi ’te atuki Nzambe ; esengeli ’te baboma ye na mabanga (tala Lv 24,16).
9,2. Tala Lk 13,2 (Bob 20,5 ; 34,7 ; Yob 9,3).
9,3. (na likamwisi Yezu akosala).
9,4. Ntango Yezu azali naino na bomoi o nse, azali lokola moi : akoki koteya bato ; mokolo awei, alongwi o nse, ekomi butu : akoki kosala lisusu te.
9,14. O miso ma ba-Farizeo bakoki kosalisa moto bobele soko azali na mpasi makasi.
9,22. Ba-Farizeo bakobengana o sinagoga moto nyonso ayambi ’te Yezu azali Mesiya, mpo aboyi mambi ma boyambi bwa bankoko.
9,24. Ezali lokola ndai balayisi ye mpo ’te aloba makambo ma solo, mpe abongisa botuki bwa ye, mpo alobaki ’te Yezu azali profeta.
9,39. Yezu ayei kokitisa bato te, ayei kobikisa bango (tala 3,17 ; 8,15) ; kasi mpo ya (misala mpe mateya ma) ye bato ba boyambi bakomi kokabwana na baye bayambi te (tala mpe 3,18 ; 15,24).
10,1. Mbala mingi o buku ya Bondeko bwa Kala ‘etonga ya mpata’ elakisi ekolo ya Nzambe : Nzambe akokengeleke bato ba ye (tala Iz 40,11).
10,14. Nayebi : nayebi bango malamu, nakolingaka bango : Tala Oz 2,22 ; 6,6 ; Yo 14,17 ; 17,3 ; 1 Kor 8,3.
10,16. Bapagano baye Yezu akoka­mba o etonga ya mpata ya ye. Alingi koyingisa bango o ekolo ya ba-Yuda te, kasi kosangisa bango o etonga ya ye mpo ya kokamba bango o bomoi bwa lobiko.
10,17. Na bonsomi mpe na bolingi bonso Yezu akabi bwa ye, kasi asekwi nsima ya mikolo misato.
10,22. Eyenga ya bobenisi Tempelo : 0 1 Mak 4,52 mpe o 2 Mak 2,20 ; 10,6 tokotanga ’te Yuda Makabe atii eyenga eye mpo ya kokundola mbula na mbula boniboni babongisi mambi manso ma bokumisi Nzambe.
10,23. Nsima ya Nkitela ya Elimo Santu bakristu ba yambo bakosangana kuna elongo na bapostolo.
10,34. O Nz 82,6 bazuzi batangemi banzambe, mpo bakosambisaka bato o nkombo ya Nzambe.
11,9. Bomoi bwa Yezu bozali lokola mokolo moko : ngonga 12 ; bato bakoki kolongola ndambo te. Yezu akokufa bobele soko elaka Tata atii ekoki. Bongo akokende o Yeruzalem, bakoki koboma ye naino te.
11,10. Kala bato bakanisaka ’te mwinda mokopelaka mpe o kati ya liso.
11,16. Lipasa : o lokota la ebrei : Toma.
11,25. –Nazali nsekwa... : Yezu amiyebisi lokola liziba lya bomoi ; mpo ya ye bato banso bakosekwa.
–Soko molimo mwa moto mokufi mpo ya lisumu, Yezu akoki kozongisela ye bomoi mpo ’te azwa bomoi bwa lobiko.
11,49. O mobu mona : Kaifa azalaki nganga mokonzi banda 18 tee 36 nsima ya mbotama ya Kristu.
11,51. Nganga mokonzi alobi ya solo ’te Yezu akokufa, kasi abungi ntina ya liwa lya ye : Yezu akokufa te mpo bakanaki koboma ye to mpo ya kokitisa mitema mya ba-Roma, kasi mpo Yezu ye moko akani kokokisa mosala moye Tata apesi ye.
11,52. Yezu akufa mpo ya ba-Yuda mpe mpo ya bato banso.
11,54. Efraim : mboka mosika na Yeruzalem km. 20 (o Esti).
11,55. Liboso lya kolia mpata ya Pasika ba-Yuda bameseni komipetola, basengeli kolongola mbindo inso ba­zwaki na bokutani na bapagano. (Tala mpe 18,28).
12,3. Mpo ya kopesa lokumu na moto ayei kotala bango ba-Yuda bazalaki kosukola ye makolo mpe kosopela ye malasi o motó. Kasi Maria oyo asopeli Yezu mpe malasi o makolo, mpe apa­ngusi mango na nsuki ilai ifungwani, mpo ya kokumisa Mokonzi wa ye.
12,7. Yezu ayebaki ’te liwa lya ye likomi pene. Yango wana atali maye Maria asali awa lokola elembo ya lokumu loye alingi kopesa ye o mokolo bakokunda ye.
12,15. Bakomi : ‘Elenge mwasi’ Sion. O Bikoma Bisantu bakotangaka Sion (Yeruzalem) ‘elenge mwasi’ lokola ’te : ‘wa bolingo’ wa Nzambe. Tala Iz 62, 11 ; Za 9,9. Yeruzalem etongamaki o mokongo ya ngomba lokola mwana akokangemaka o mokongo mwa mama.
12,23. Tala Yo 2,4. Elaka ya Yezu ekomi pene.
12,27. Yezu asambeli awa lokola o Getsemani. Maye makweli Yezu kuna Yoane akomi mango te.
12,31. Mokonzi wa nse : zabolo.
12,38. Na maloba ma Iz 53,1 elobami ’te Nzambe asili ayebi ’te bakoza­nga boyambi.
12,39. Bazangi boyambi, lokola Izaya alobaka.
12,40. –Tala Iz 6,9-10.
Ezali etumbu ya nsomo : soko moto aboyi ngrasya ya Nzambe, Nzambe akangi miso mpo ’te abika lisusu te. 
12,41. Ntango Izaya amoni o limoni nkembo ya Yawe o Tempelo (Iz 6,1-4) atali yango lokola nkembo eye Kristu akozwa. Tala mpe Yo 8,56.
13,5. Kosukola makolo ezalaki mosala mwa basaleli. Yezu, awa asali yango, alingi kolakisa bayekoli ’te bamikomisa basaleli ba baninga, mpe basalela bango na motema bolingi, lokola bato batindami na makasi te.
13,8. Petro alingi te ’te Yezu asala mosala mwa moombo ; ayebi te ’te Yezu ayei kosalela bato (Mt 20,28 ; Mk 10, 45).
13,26. Mbala mingi o limpati lya eyenga ya ba-Yuda nkolo wa ndako akamati eteni ya limpa to ya mosuni, atii yango o supu mpe apesi yango na mobyangemi wa lokumu oyo alingi kokumisa. Awa Yezu asali yango na Yudasi, alingi kokebisa ye na boku­ndoli maye makomami o Nz 41,10.
13,31-32. Yezu akondima koyoka bolozi. Na botosi ndinga ya Tata wa ye akokembisa Tata, mpe Tata akokembisa Mwana o mokolo akosekwa o bawa.
13,36. Na bobombami Yezu ayebisi Petro ’te akokufa mokolo mosusu lokola Mokonzi wa ye.
14,2. To : Soko ezalaki bongo te, mbele nayebisaki bino te ’te nakoke­nde kobo­ngisela bino esika.
14,3. –Bozongi bwa Kristu : Tala mpe 1 Tes 4,16 ; 1 Kor 4,5 ; 11,26 ; 16,22 ; 1 Yo 2,28 ; Boy 22,17.20.
–O mboka Tata.
14,6. Yezu azali nzela, zambi bobele moto akoyamba Yezu akokende epai ya Tata. Yezu azali Bosolo, zambi bobele ye ayebi Bosolo bwa Tata mpe akoyebisa bwango. Yezu azali Bomoi (bwa lobiko), zambi moto ayebi Yezu, ayebi mpe Tata (tala mpe 17,3).
14,18. Nsima ya liwa mpe nsekwa ya Yezu, bato baboyi Nzambe, bakomono ye lisusu te ; bobele bayekoli ba ye bakomono ye.
14,19. Nazali na bomoi (bwa lobiko mpe nakopesa bwango na bato banso bayambi ngai).
14,28. Tata aleki ngai (sikawa naino) na bonene : Tee nsekwa ya ye, nkembo ya Yezu ekobombama naino.
14,30. Satana moto azali kokamba Yudasi na bakambi ba ba-Yuda o etumba ya bango na Yezu mpe na mosala mwa Nzambe.
15,4. Bayekoli ba Yezu bayamba mpe balinga Yezu mpo ’te bakoka kobota mbuma ya bosembo.
15,16. Soko tokosambela o nkombo ya Yezu tokosenge ata nini te, kasi tokosa­ngisa losambo la biso na maye Yezu alingi. Mbala mingi Yezu azalaki kosambela mpo ’te Bokonzi bwa Tata boya. 
Tala malako masusu ma Yezu o Lk 11,5-13 ; 69,5.
15,18. Bato ba nse eye : baye balingi makambo ma mokili.
16,9. Bakanisi ’te bazali na lisumu te ; kasi awa bayambi ngai te, bazali na lisumu linene.
16,10-11. Bakanisi ’te nazali profeta wa lokuta. Nzambe atiki ngai, kasi nauti na Nzambe mpe nakozonga o mboka Tata. Bakanisi ‘te, awa zabolo atindi bango baboma ngai, zabolo akolonga ; kasi na kuruse ya ngai nako­kweisa zabolo.
16,14. Na misala mya ye Yezu azalaki kokumisa Tata oyo atindi ye. Elimo akokumisa Yezu mpo ya mambi ma bosolo azwaki epai ya Mwana mpe epai ya Tata.
16,16. Nsima ya nsekwa.
16,23. O mokolo mona : nsima ya nse­kwa ya Yezu mpe Nkitela ya Elimo Santu.
16,30. Bayekoli basusu babandi komitunatuna (16,17). Awa Yezu alakisi ’te ayebi makanisi ma bango, bango baya­mbi ’te auti na Nzambe.
17,1. Esilisi Yezu kopesa malako na bayekoli, abandi koloba na Tata wa ye : abondeli ye mingi asantisa bato ba ye, mpo ’te mokolo moko banso bakoka kosangana seko o Bokonzi bwa bolingi.
17,9. Mbala mingi Yezu atangi ‘nse’ bato ba nse, bato bayambi Nzambe solo te, baye bakotosaka ye te, banguna ba Nzambe mpe ba bakristu. Biso, soko tozali kotosa Nzambe, tokoki kokangema na nse te (m. 11). Yezu alobi : bazali o nse, zambi bazali bobele o ntei ya bato babe.
17,19. Bakomi : ‘nakomisantisa’ lokola ’te : nakomibonza, bongo bakoki koboma ngai.
17,20. Bakristu banso bakoyamba Yezu tee nsuka ya molongo.
18,1. Mai ma Sedron makaboli ngomba ya Oliva na Yeruzalem.
18,15. Moyekoli mosusu : Yoane ye moko. Tala mpe 19,26 ; 20,2 ; 21,24.
18,24. Ana azalaki nganga mokonzi o mibu 6-15 nsima ya mbotama ya Kristu (Lk 3,2). O mokolo bakambi Yezu epai ya ye, atikali se nganga mokonzi ‘wa lokumu’. Yango wana atindi Yezu epai ya Kaifa oyo azalaki nganga mokonzi o ntango ena.
18,28. Pilato azali mopagano, moto mbindo. Soko mo-Yuda akoti o ndako ya ye akomi na mbindo.
18,34. Ntina ya motuna mwa Yezu : « Yo mpenza okanisi to oyoki ’te ngai natombokeli Kaizaro ; to otuni ngai zambi ba-Yuda bafundi ngai na likambo liye ? »
18,37. « Nasakola bosolo » lokola ‘te : « Nasakola mateya ma solo na mokano mwa solo Tata akanaki mpo ya bolamu bwa bato. » Pilato ayoki ntina ya maloba maye te.
19,7. Bafundi Yezu ’te atuki Nzambe (10,33-36) ; engebene na mobeko (Lv 24,26) akoki na etumbu ya liwa.
19,8. Mpo amoni ’te Yezu azali ata nani te, Pilato, moto wa biyambaya­mba, akomi kobanga maye makoki kokwela ye.
19,9. Outi wapi : Awa Pilato atuni ekolo ya Yezu te. Atuni ye bongo zambi ayokaki (19,7) ’te ba-Yuda bafu­ndi ye mpo alobaki ’te azali Mwana wa Nzambe.
19,13 : ‘Litostrotos’ : esika batanda mabanga manene.
19,26-27. Soko ya solo Yezu azalaki na bandeko ‘tata moko mama moko’, ndenge nini Yezu akokaki kolakisa Maria awa lokola mama wa Yoane (mwana wa Zebedeo) ?
19,30. Mosala mwa kobikisa bato.
19,35. Yoane ye moko.
20,8. Yoane akomi na boyambi bwa solo o ngonga enamei
20,17. Osimba ngai te : Yezu alobi bongo, mpo nsima ya nsekwa ya ye akomi na nzoto ya nkembo.
20,23. Na maloba maye Yezu apesi bakonzi ba Eklezya likoki lya koli­mbisa masumu.
20,31. Mbele Yoane asilisaki Nsango Elamu awa, mpe abakisaki eteni ya 21 na nsima.
21,11. Mbisi 153 : O eleko Yoane akomaki Nsango Elamu eye bato bayebaki bobele ndenge 153 ya mbisi. Bongo ‘mbisi 153’ lokola ’te mbisi ya ndenge inso. Mbisi elakisi ndenge inso ya bato : bato ba bikolo binso, bato ba lokumu, bato ba nkita, babola, bato bake, b.n.b.
21,17. –Yezu atuni Simoni mbala isato. Ayebisi ye bongo ’te alimbisi ye ndenge awangani Mokonzi wa ye mbala isato.
–Yezu apesi Petro bokonzi bwa Ekle­zya ; alakelaki ye bwango liboso (Mt 16,18).
21,18. Yezu asangeli Petro ’te, o boba­nge bwa ye, akosengela kokufa mpo ya Mokonzi wa ye. « Esika okolinga kokende te » elakisi esika bakoboma ye.
21,19. Petro akokufa o kuruse o Roma.
21,24-25. Mbele eteni eye ekomami na Santu Yoane te, kasi na moto oyo Yoane atangeli maloba ma buku eye mpo ’te akoma mango o monkanda : andimi bongo kozala nzeneneke wa manso makomami awa.