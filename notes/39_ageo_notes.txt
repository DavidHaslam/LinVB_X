AGEO


1,1. – O 520 liboso lya Y.K., mibu 18 nsima ya bozongi o Yeruzalem bwa bato ba liboso bautaki o boombo o Babilon.
 –Tala Esd 2,2 ; 3,2.8 ; 4,1.
1,2. Tala Esd 3,1 - 4,24.
1,14. Bato bakondimaka Nzambe na motema moko batikalaki mingi te.
2,3. O kati ya bato bazalaki koyoka Ageo, babange basusu bayebi naino ’te mibu 67 mileki babebisaki Tempelo : bakokoka lisusu kotonga yango na nkembo inso te lokola ezalaki liboso, nzokande Ageo alobi : Nkembo ya ndako eye ekoleka eye ya ndako ya kala, mpo Yawe akotia boboto wana (2,9). Ayebi ’te bato bakoya na biloko mingi bya motuya o Yeruzalem (Iz 60, 5-17 ; 66,12).
2,10-14. Sanza isato ileki babandi kotonga Tempelo lisusu. Ageo alobi : Bato baye bazali na mbindo, se lokola moto asimbi ebembe : binso akobonzela Nzambe bikozala na mbindo. Mbele alobi bongo mpo ya kokebisa bato : balembe mosala mwa botongi te, soki te bokozala na mbindo o miso ma Nza­mbe, mpe mabonza ma bino makokoka kosepelisa ye te !
2,20. Nkumu oyo azalaki mwana wa Sealtiel (1,1), oyo azalaki mwana wa mokonzi Yoyakin (1 Mik 3,17). Zorobabel azalaki bongo nkoko wa Davidi. Ageo, lokola bato basusu baike, azalaki na elikya ’te Zorobabel akokoma mokonzi wa ekolo. Ba-Persi batiaki ye moyangeli wa Yuda ; Ageo ayebisi ye ’te Yawe azali na ye, yango wana akosala makambo manene mpo ya ekolo ya ye.
2,23. Bato banene bazalaki kolata mpete, eye ezalaki na elembo (elili to nkombo). Bazalaki kotia elembo eye mpe o minkanda mya bango, mpo ya kolakisa ’te bango bato bakomaki bo­ngo.