MALAKIA


1,1. Yawe na profeta basololi na bato : bato balobi ’te balembi mpo ya kwokoso ya makambo ma mokili ; mbele ba-Edom, banguna ba bango ut’o kala, bayokisaki bango mpasi. Babandi lisusu komilengele mpo babu­ndisa ba-Israel. Yango wana bato bazalaki komilela « Yawe alingi biso lisusu te » ; kasi Nzambe ayanoli : « Ut’o kala nakolingaka Yakob (Yuda) leka Ezau (Edom) ».
1,6. Sikawa Nzambe na profeta basololi na banganga Nzambe, baye bakosalaka mosala mwa bango malamu te. Bakamwa nde soko bamoni ’te Nza­mbe asepeli na bato ba ye te ?
1,11. Profeta alingi ’te babongisa milulu mya losambo o Yeruzalem, mpo ’te bato ba bikolo bisusu bakoka komono epai ya ba-Yuda ndakisa ya solo ya bokumisi Nzambe.
2,7. Mateya matali Mobeko na ndi­mbola ya mwango.
2,10. Nzambe apaleli bato mpo bamemyi bondeko te, boye Nzambe akataki na bango o Sinai mpo ’te batosa Nzambe mpe balingana bo bandeko. O kati ya bango mingi babalaki basi bampaya baye babendi babali ba bango o nzela ya bokumisi banzambe ba lokuta. Nsima ya boombo mabala maye mapekisamaki (Ez 10,1-44 ; Ne 13,23-27). Nzambe asepeli mpe te na bomoni ’te babali bazalaki kobengana basi ba bango, ata ntina ya solo ezalaki te ; ntango akelaki bango, mobali na mwasi, alingaki yango te (Lib 2,21-24).
2,16. Tala Mt 19,6-8.
2,17. Tala Am 2,16+ ; Yl 1,15+.
Bato basusu bakanisaki ’te Nzambe azali na bosembo te mpo akondimaka ’te bato babe bazala na bomengo. Profeta ayanoli bango ’te mokolo Nzambe akomilakisa akokata makambo manso na bosembo. Liboso akotinda ntoma wa ye, na nsima ye moko akoya mpo asambisa basumuki, banganga Nza­mbe liboso, bato basusu nsima. Profeta ayebi malamu te ndenge nini yango ekosalema, kasi ayebi ’te akolongisa bosembo. Yango ezali mpe elikya ya biso : eleko eye Nzambe akokata makambo manso esili ebandi na boyei bwa Yezu Kristu.
3,6. Bato balingi kokosa Nzambe ’te bayebi te na makambo nini basalaki mabe, mpe ndenge nini basengeli kozongela ye. Nzambe ayanoli : Soko botosaki ngai o makambo manso, mbele nabenisaki bino, napimaki bino mbula te, mpe nabebisaki mbuma ya bilanga bya bino te.
3,16. Na elili eye ya buku mokomi alingi kolakisa ’te Nzambe ayebi bato ba bosembo : banzelu ba Nzambe o likolo bakokomaka nkombo ya base­mba banso o buku enene.