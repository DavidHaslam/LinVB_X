FILEMO


1,1-3.	–Tala Rom 1,1-7 ; 1 Kor 4,17.
–Mbele Apia azali mwasi wa Filemo ; Arkipo akoki kozala mwana wa bango.
1,6. Mpo ya boyambi bwa ye mokristu asangani na Kristu mpe na bandeko bakristu. Boyambi bosengeli kobimisa misala ; na myango mokristu akopesa ndakisa ya boboto mpe ya bolingani. Akabela baninga bongo makabo maye Kristu akabeli ye.
1,10. Onezimo : Tala ndimbola o ebandela ya monkanda moye. Ntina ya nkombo Onezimo : ‘Moto azali na ntina’.
1,16. Ata Onezimo atikali moombo, Filemo atala ye lokola ndeko wa ye, zambi o miso ma Nzambe nkolo azali te, moombo mpe te.
1,19. Ozali na nyongo ya ngai : Filemo akomi mokristu na lisalisi lya Polo ; mpo ya ye azwi boyambi mpe akuli batisimo.
1,24. - Marko : Tala 2 Tim 4,11.
- Aristarko : Tala Bik 19,29 ; 27,2 ; Kol 4,10.
- Demas : Tala Kol 4,14 ; 2 Tim 4,10.
- Luka : Tala Kol 4,14 ; 2 Tim 4,11.