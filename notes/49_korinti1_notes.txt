KORINTI 1


1,2. Awa mokristu abyangemi na Nzambe mpe akuli batisimo na boyambi, asili akomi ‘mosantu’, ayambami o ekolo ya Nzambe ; bongo abyangemi mpo ’te akokisa bosantu bwa ye o bomoi bwa ye mobimba.
1,5-6. Makabo ma koloba mpe ma koyeba : Tala 12,1.
1,8. Mokolo Kristu akoya na nkembo ya ye inso (tala 15,23 ; Fil 3,20 ; Kol 3,4).
1,12. Apolos : Tala Bik 18,24 - 19,1.
1,17. Bwanya : Moto andimi se bwanya bwa bato (koyeba koloba malamu mpe kolimbola makambo na ntoki, lokola ba-Greki bazalaki kosala), azali koboya ngrasya ya Nzambe, eye ase­ngeli na yango mpo ya kotongolo Bwa­nya boye Nzambe alakisi na liwa lya Mwana wa ye o kuruse. Se na mayele ma biso bato tokoki koyeba Bwanya bwa Nzambe te. Solo, Nzambe apesi bato mayele, bongo akoki koboya mango te, kasi alingi te ’te na lofundo la bango baboya Bwanya bwa Nzambe.
1,21. Bwanya bwa Nzambe bomononi o bikelamo bya ye binso.
2,6. –Bato bakomi bakolo o mambi ma eyamba : baye bandimi ’te Elimo akamba bango.
–Baye bakoyangelaka mokili moye : to bakonzi ba leta, to baye bakolukaka ’te bato baboya libiki liye Kristu ayeli bango.
2,14. Moto azangi Elimo : Oyo aboyi kokambama na Ye mpe azali kotia motema se na ye moko.
3,5. Mosala mwa bateyi : kosangela Nsango Elamu mpo ya lokumu la Kristu. Bamilakisa bakonzi te, bawela lokumu la bango moko te.
3,13. Mokolo mona : Mokolo Mokonzi akoya kokata likambo lya bato banso (tala 4,5).
4,1. Mibombamo mya Nzambe : makabo maye Nzambe apesi bato na mateya mpe na bomoi bwa sika, boye Kristu ayeli bango.
4,8. Bosili botondi : Polo aseki bango mpo ya lolendo la bango, mpo basali lokola basili kozwa nkembo ya seko.
4,16. Bamekola ndakisa ya Polo, lokola Polo azali komekola ndakisa ya Kristu (11,1).
4,17. Tokoki kotanga mosala moye mwa Timoteo o Bik 19, 21-22.
5,1. Kofanda na mwasi wa tata wa ye : Likambo liye lisili lipekisami o Bondeko bwa Kala (Lv 18,8).
5,5 : Polo akolikya ’te moto oyo, soko azwi etumbu, akoki kobongola motema.
5,6-8 : Lokweza (nkisi ya mampa) : Polo alakisi lango awa lokola eloko eye ekobebisaka ezalela ya moto (tala mpe Gal 5,9). O eleko ya Pasika ba-Yuda bakoki kolia bobele mampa malambami na nkisi eye te. Balongola mpe o ndako ya bango nkisi inso ya mampa mpo ’te balia mpata ya Pasika na motema peto. Se bongo bakristu basengeli kolongola o mitema mya bango ntina inso ya bobe (masumu) mpo ya kolia Pasika ya sika.
5,9. Liboso lya kokomela ba-Korinti monkanda moye, Polo asili atindeli bango monkanda mosusu mpo ya koyanola mituna misusu mya bango ; kasi monkanda mona mobungi.
6,1. ‘basantu’ : bakristu (tala 1,2).
6,2. Kristu akokata makambo ma bato banso, basantu bakozala na ye elongo.
6,11. ‘Bosukwami na mai’ (ma batisimo).
6,12. « Nakoki kosala manso nalingi kosala » : Mbele maloba bato basusu bayokaki o monoko mwa santu Polo mpo ya mambi ma botosi Mobeko mwa Moze ; kasi sikawa bakomi koloba bongo mpo ya makambo manso. Na bokuse Polo ateyi biso awa ezalela ya mokristu : Koyeba nini ezali malamu mpe nini ezali mabe ekoki te ; mokristu asengeli kokana kosala maye makokolisa boyambi bwa ye.
6,20. Basombi bino kosomba : Basikoli (Nzambe asikoli) bino na motuya monene (tala Rom 3,24 ; 14,9).
7,1-40. Polo apesi awa mateya manso ma libala te, ayanoli se mwa mituna. Baye azali koteya bazali bakristu ba sika, bakofandaka o mboka eyebani mpo ya bizalela bya mobulu bya bato. Basusu bazalaki mpe koteya ’te bato basengeli kotika manso matali libala. Mpo ya koyeba ntina inso ya libala, osengeli kotanga mpe Ef 5,22-33.
7,7. Polo abalaki te, azalaki monze­mba.
7,12. Bato basusu : Baye balongani na moto azali mokristu te.
7,17. Moto akomi mokristu atikala lokola azalaki : Kanisa maye matali ndako eye afandi, mosala azali kosala, lifuta bazali kofuta ye, b.n.b.
7,21. Basusu babongoli boye : ‘Ata okoki kokoma na bonsomi, malamu ondima kotikala moombo mpe ozwa bongo litomba lya boombo’.
7,23. Na bolandi makanisi mpe bizalela bya baye bakolandaka mposa ya motema.
7,25. Bangondo : Soko mobali to mwasi atiki libala, mpo alingi komipesa mobimba na mosala mwa Nzambe (tala Mt 19,12).
7,26. Bakristu baike bazalaki koyoka mpasi mpo ya boyambi, basusu bawei liwa lya martiro. Kwokoso eye mokristu monzemba akoyoka, ekokwela bato basusu (mwasi na bana) te.
7,29. Tokangema na biloko bya mokili te, zambi Mokolo mwa Boyei bwa Mokonzi mozali lisusu mosika te.
8,1-13 : Bapagano bazalaki kobonzela bikeko nyama, kasi mbala mingi bazalaki koteke eteni enene ya nyama eye o zando (10,25), to kolia yango penepene na tempelo (8,10). Bakristu bakoki kolia nyama ena, kasi bapesa esimbisi te na baye bakobangaka kolia yango (8,1-6 ; tala mpe 10,25-30). Polo alobi se bongo o Rom 14-15. Nzoka­nde Polo azali kopekisa bakristu kosangana na bapagano esika bakobonzelaka bikeko nyama (8,10 ; 10,14-22).
9,5. Kokende na biso (mpo ya kosalisa biso).
9,6. Barnaba azalaki moninga wa Polo o mobembo mwa ye mwa yambo (Bik 11,25-26 ; 13,3 - 14,28).
10,1. Ndakisa ya makambo makwelaka ba-Yuda o eliki (tala Bob 13,21 ; 14,22) ekebisa bato ba Korinti : Ata Nzambe alakisi bango makamwisi manene (lokola bilembo bya basakrame­nto : 1-4) bankoko ba biso baboyaki Nzambe, bakumisaki bikeko mpe bakufaki.
10,3. Bilei biuti o likolo : Manu (tala Bob 16,4-35 ; Mbk 8,3 ; Yo 6,49).
10,4. Yawe amilakisaki lokola ‘Libanga lya ba-Yuda’, mobateli wa bango (Bob 12,5-6). ‘Libanga lya bakristu’ lizali Kristu, liloba lya Nzambe.
11,2-16. Ut’o ebandela, esika bakristu bakosanganaka, basi basengeli kolata litambala o motó, se lokola basi ba-Yuda. Ekomi lokola « mobeko » mwa Eklezya. Kasi o Korinti basi basusu baboyi kozipa motó, yango wana Polo apaleli bango. Toyeba ’te Polo alobi bongo na bato ba eleko ya ye ; ekoki kozala mobeko mwa seko te. Bato basengeli kolanda mimeseno mya ekolo ya bango.
11,10. Mwasi asengeli kolata elembo ya botosi : litambala.
11,17. Ut’o ebandela bakristu bakosanganaka o ‘limpati lya bondeko’ : bazali kolia, kosambela, koyoka mateya mpe kolia Limpati lya Pasika.
11,21. Biloko bya ye moko : Biloko 
biye ye moko amemeki, biye asengelaki kokabola ye na bato basusu.
12,1. Makabo ma Elimo : makabo maye Elimo Santu akokabelaka moto moko moko, mazali ndenge na ndenge (12,8-10.28-30). Bakristu ba Korinti bazalaki kokumisa makabo mana mingi, liboso mpenza maye mazalaki kokamwisa bato ; bazalaki koluka na mango bolamu bwa bato banso te ; basusu bazalaki kosangisa mango na makambo ma bopagano. Yango wana Polo ayebisi bango eloko nini ekolakisa bosolo bwa makabo ma Elimo Santu (12,2-3). O 12,4-31 alakisi ’te makabo manso mauti se na Elimo mpe mazali se na ntina ya kosalisa Eklezya mobimba, mazali na ntina ya komisalisa te. 
Yango wana likabo lya kosakola lokola profeta lileki likabo lya koloba nkota (14,1-25), nzokande bolingi boleki 
makabo manso (13,1-13).
Na nsuka Polo apesi bango mibeko 
miye misengeli kobongisa makita ma 
bakristu (14,26-40).
12,9. Boyambi boleki bonene : tala 13,2.
12,10. Koloba Liloba lya Nzambe : 
baye bakosakolaka na lisalisi lya Elimo Santu mpo ya kolendisa mpe kobondo bato bazali koyoka bango.
12,31 - 13,13. ‘Nzembo ya bokumisi bolingi’ ezali na biteni bisato : 
– 1-3 : bolingi boleki makabo manso,
– 4-7 : mbuma bolingi bokobotaka,
– 8-13 : bolingi bozali bwa seko.
13,1. Bolobi nkota bozali likabo lya Elimo, kasi bozali se na litomba soko Elimo apesi mpe likabo lya kolimbola nkota.
13,3. ‘Ata nandimi kokufa na móto’ to nakaba nzoto ya ngai mpo ’te bakumisa ngai.
13,12. –Tala Gal 4,9.
–To : lokola basusu bayebi ngai.
14,21.	–Tala Iz 28,11-12.
–Ekomami ‘o Mobeko’, lokola ’te ‘o Minkanda Misantu’.
14,22. Ntina ya molongo moye ezali polele te ; o milongo mya nsima (23-25) Polo alimboli makanisi ye polele.
14,34. Polo apekisi basi koloba te ; kasi alingi te ’te bamilakisa o makita te. Mbele basi basusu balingaki kolanda ezalela ya basi ‘banganga-nzambe’ ba Korinti, baye bazalaki na mosala monene o tempelo ya nzambe-mwasi Afrodite.
15,2. Polo akebisi bango ’te basengeli koyamba mateya ma Yezu manso, ná nsekwa ya bawa ; yango wana akundoleli bango ndenge Yezumei asekwi.
15,4. (nsima ya liwa lya ye).
15,13. Moto ayambi nsekwa ya bawa te, aboyi mpe koyamba ’te Yezu ase­kwi o bawa. To : Yezu asekwi mpo ’te biso banso tosekwa nsima ya ye (tala mpe 15,20-23).
15,21. Lokola ekomami o Lib 3,17-19.
15,29. Polo atangi awa momeseno mwa bakristu ba Korinti, kasi biso toyebi ntina ya mwango te.
15,32.	 –Tokotanga ndakisa ya likama liye (etumba na nyama) bobele awa. Ata o 2 Kor 11,23-26, esika Polo atangi makama ndenge na ndenge makwelaki ye, atangi likama liye te. –Tala Iz 22,13.
15,33. Eyele ya Menander, mokomi mo-Greki.
16,22. ‘Maranata’ : Elobeli ya ba-Aram. Na maloba maye (« Mokonzi wa biso ayei », to « Mokonzi, yaka ») bakristu bazalaki kolakisa boniboni bazalaki kotia elikya na Boyei bwa Mokonzi. Tala mpe Boy 22,20 ; Rom 13,12 ; Fil 4,5 ; Yak 5,8 ; 1 Pe 4,7.