YEREMIA


1,1. Mboka eke o Nordi ya Yeruzalem.
1,3. Sedekia azalaki mokonzi banda 597 tee 587 (nsuka ya Yeruzalem).
1,5. Yawe asili aponi Yeremia mwa kala mpo azala profeta. Yango esalemi mpe na Samson (Baz 13,5), na Yoane Mobatizi (Lk 1,15.41), na Polo (Gal 1, 15), mpe na Yezu (Lk 1,35).
1,7. Yeremia ayebi ’te basengi ye mosala monene mpenza ; akundoli maye Izaya ayanoli Nzambe : « Ngai oyo, tinda ngai » (Iz 6,8). Lokola Moze, ali­ngi aboya (Bob 3,11.13 ; 14,1.10.13). Mikolo misusu akoyoka nsomo mpe akolembe (12,1-4 ; 5,10-18 ; 20,14-18), kasi Nzambe akobatela ye ntango inso, bongo o makambo ma mpasi mpe ma kwokoso Yeremia akozala moto wa mpiko ya solo.
2,5. Bikeko biye bato ba Israel bazalaki kokumisa ut’o ntango bakomaki o Kanana : moto akumisi bikeko biye akokoma lokola byango (Oz 9,10 ; Nz 115,8 ; 135,18 ; 2 Kor 3,18).
2,10. –Na nkombo eye bazalaki kolakisa liboso ba-Kitim o esanga Sipro, na nsima bato banso ba bisanga bya Mbu Enene ya Westi.
–Kedar ezalaki eteni ya Nordi ya Arabia ; ‘banda Kitim tee Kedar’ lokola ’te : o bikolo binso biyebani.
2,16. Memfis na Tapanes : bingumba bya Ezipeti biye Yeremia akotangaka mingi. Lokola Izaya, Yermia mpe ali­ngi te ’te baluka lisalisi lya bikolo bisusu, Ezipeti to Asur : batia mitema se na Yawe (Iz 20,2-6 ; 30,1-7 ; 31,1 ; Oz 5,13 ; 7,11).
2,20. Ndako ya banzambe ya ba-­Kanana, Baal na Astarte, izalaki koto­ngama mingi o nsonge ya mwa ngo­mba, to o lobwaku pene na liziba, to pene na nzete enene mpe kitoko.
2,21. Tala mpe 5,10 ; 6,9 ; 8,10. Na moulani moye Yeremia akomi lokola Ozea (10,1) mpe Izaya (5,1-7).
2,23. Mbele balakisi awa lobwaku la bana ba Inom (Yoz 18,16) o Sudi ya Yeruzalem, esika bazalaki kosala makambo ma nsomo o ndako ya banza­mbe ba bango.
3,1. Epekisamaki (Mbk 24,1-4) ; nzokande Nzambe, mpo ya bolingi bonene akolingaka ekolo ya ye, alingi kokamata ‘bolongani wa ye’, soko abongoli motema.
3,8. Yeremia alakisi bongo ’te bakobebisa Samaria na ekolo mobimba ya Israel, soko babongoli mitema te (tala Oz 1-3).
3,14. Maloba maye (3,14-18) makomami nsima ya nsuka ya Yeruzalem (587), ntango bazalaki na Sanduku ya Bondeko lisusu te (3,16), mpo ba-­Babilon batumbaki yango elongo na Te­mpelo. Yeremia alakeli bango ’te, ntango Yawe akobongisa ekolo, bakosengela na Sanduku ya Bondeko lisusu te bo ele­mbo ’te Yawe azali wana, mpo akokomisa Yeruzalem engumba nta esan­tu.
3,22. Na monoko bandimi kobongola mitema, kasi o mitema bakanisi nini ?
4,5. O limoni Yeremia amoni basoda ba banguna bakoya ut’o Nordi kokotela mokili mwa bango mobimba. Kin’o 612 ba-Asur bakaneli bango, na nsima bato ba Babilon bakokotela ba­ngo mbala mingi.
4,10. Baprofeta ba lokuta bazalaki kosa­ngela bato boboto (5,31 ; 14,13-16 ; 23,17), nzokande Nzambe apekisi ba­ngo koloba te.
4,19. O kati ya motema Yeremia asili ayoki mpasi eye bato ba ekolo ya ye bakoyoka.
5,1. Etumbu eye ekokwela bango mpo bato banso basali lisumu : bazangi bosembo mpe bakumisi bikeko. O 5,31 Yeremia afundi liboso banganga Nzambe na makambo maye.
6,1. Awa profeta alakisi ndenge ba­nguna babelemi na Yeruzalem : mobulu mokoti o mboka ya pene. 
 6,4 tokoyoka maloba ma banguna (‘Boteleme...’) mpe maye ma bato ba Yeruzalem (‘Mawa na biso...’).
6,8. Soko Yeruzalem andimi komibongisa, mabe makokwela yango te, mpo Nzambe akondimaka ntango inso kolimbisa.
6,17. Bakengeli baye ezali baprofeta ba solo baye Nzambe akotindaka mpo ya kokebisa bato ba ye (Ez 3,17el ; 33,1el).
6,20. Tala 1 Bak 10,1.
7,1. Ba-Israel bakanisaki ’te mabe moko makoki kokwela bango te, mpo Yawe azalaki o Tempelo ya ye. Yeremia akebisi bango : « Soko bobongoli mitema te, Nzambe akoki kotika Te­mpelo ya ye mpe kosukisa bino ; asalaki yango na Tempelo ya ye o Silo o nta­ngo ya bazuzi (1 Sam 1-3). Ezekiel akomono ndenge nkembo ekolongwa o Tempelo (Ez 11,23). O Yer 26 to­kota­nga ’te maloba maye ma Yeremia makokokisama.
7,18. Bazalaki kopesa nkombo eye ya lokumu na nzambe mwasi wa ba-Babilon, Astarte. O Yeruzalem bato mingi bazalaki kokumisa ye .
7,22. Yeremia abungi te ’te ba-Ebrei babonzelaki Nzambe mabonza o eliki (Bob 24,5), kasi alingi koteya bango ’te : soko batosi Nzambe te mpe bala­ndi moto mosusu, makambo masusu manso mazangi ntina.
7,31. ‘Tofet’ elakisi etumbelo ; ba-­Yebus baye bafandaki liboso o Yeruzalem bazalaki kotumbela nzambe wa ba­­­ngo Molok bana ba bango (19,5 ; 32,35), mpe ba-Israel basusu bamekolaki bango.
8,18. Kosakola makambo mingi ma mpasi na bato ba ekolo ya ye eyokisi profeta mawa mingi. O 9,1 alobi ’te alingi kokima mosala moye mpo ake­nde ye moko o eliki.
8,22. Galaad, o Esti ya Yordane, eyebanaki mingi mpo ya mafuta ma nsolo mpimbo (Lib 37,25 ; 43,11).
9,9. Yeremia amileli mpo ya mpasi enene ekweli ekolo ya ye, eye banguna babebisi o bisika mingi. Mbele atangi na yango maye basoda ba Nabukodonozor, mokonzi wa Babilon, basalaki o 605 liboso lya Y.K. (tala mpe 2 Bak 24,1). Yeruzalem ekomi na likama.
9,24. Kokatisa nzoto ezali bobele na ntina soko ezali elembo ya solo ya boyambi bwa moto ; kasi ezali na litomba lyoko te soko moto akatisi motema te, lokola ’te : soko azangi bosembo mpe aboyi makambo ma lokuta te. Awa balakisi ’te bato ba Nzambe bazali se lokola bapagano (Yer 4,4 ; 6,10 ; Mbk 10,16 ; 30,6 ; Bik 7,51 ; 1 Kor 7,19 ; Gal 5,6 ; 6,15 ; Kol 2,11 ; 3,11).
10,17. O eteni eye (10,17-22) Yeremia, mpo ya kokebisa Yeruzalem, alakisi yango lokola mwasi oyo asengeli komilengele mpo akende o boombo ! Aleli yango mpe afundi bayangeli ba yango.
10,24. O nkombo ya bato banso abondeli Nzambe ’te apesa bango etumbu mpo ’te bamibongisa, kasi abebisa bango te.
11,1. O mobu mwa 622 bazwi ‘Bo­ndeko bwa Bondeko’ (ndambo enene ya buku ya Mibeko) ; yango wana mokonzi Yozia abongisi milulu mya bokumisi Nzambe (2 Bak 22,8-23). Maloba ‘Mabe makwela moto oyo aboyi koyoka maloba ma Bondeko’ masili makomami o Mbk 27,26 ; maloba masusu mingi totangi o 11,1-12 mauti mpe na Mbk.
11,18. O lisolo liye na Nzambe, Yeremia amileli mpo ata bandeko ba ye baboyi mateya ma ye. Masolo masusu Yeremia asololi bongo na Nzambe makomami mpe o 11,18 - 12,5 ; 15,10-21 ; 17,14-18 ; 18,18-23 ; 20,7-18.
12,1. Ndenge nini, na bosembo bwa ye, Nzambe akoki kolingisa ’te bato babe bazala na bomengo ? Akotumbola bango te ? Mokomi wa buku Bwa­nya mpe buku Yob amituni mpe bo­ngo. Yeremia atiki likambo lyango o maboko ma Nzambe.
12,5. Nzambe ayanoli motuna moye mwa Yeremia te. Tokokuta eyano eye bobele o bikoma bya nsima : Nzambe akopesa basemba mbano mpe akotu­mbola bato babe nsima ya liwa. Awa alobi na Yeremia : Osili olembi na mwa likambo sikawa, nzokande makambo manene makokwela yo.
13,1. Baprofeta bazalaki kobakisa mbala esusu mwa ekela na maye bazalaki koteya mpo ’te baye bayoki bango bayoka ntina ya mango malamu, mpe mpo ya kolakisa ’te ekosalema solo lokola basakoli. Yeremia asali mpe bo­ngo : Israel azali lokola mokaba kitoko, moye Nzambe aponi mpo ’te profeta akanga mwango o loketo ; kasi mokaba moye mofungwani (Israel aboyi Yawe) mpe mobebi (lokola Israel amibebisi na bokumbameli bikeko). Tala mpe 18,1-12 (mosali mbeki) ; 19,1-15 (mbeki ebolani) ; 24,1-10 (bikoló bya figi) ; 27-28 (ekangiseli). Tokoki koloba ’te bomoi bwa ye mobimba bozalaki lokola elembo ya maye Yeremia ateyaki.
13,18. Yeremia asakoli maloba maye liboso ’te mokonzi Yoyakin akende o boombo, elongo na bato basusu, o 598 (2 Bak 24,8-16). Mibu 11 na nsima, nta­ngo babebisi Yeruzalem, ba-Babilon bakambi ba-Israel ebele o boombo.
13,27. Makambo ma nsomo : bikeko bya banzambe.
14,1. O eteni ya 14 tokotanga ’te mpela enene eyokisi bato banso mpasi mingi : bato bamileli o miso ma Nza­mbe (7-9) mpe Yawe ayanoli bango (10). Na nsima tokoyoka lisolo lisusu lya Yawe na Yeremia (11-18), mpe na nsuka mabondeli ma bato (19-22).
15,1. Bitumbu, biye bikoleka etu­mbu ya nzala enene, bikokwela bato, mpo basali motó makasi.
15,4. Mokonzi Manase angalisi makambo ma bokumbameli bikeko (2 Bak 21,1-18).
15,10. Lisolo lisusu kati ya Yawe na Yeremia (tala 11,18) : Yeremia alembi nye : amituni soko akozala na nguya ya kosala maye Nzambe asengi ye.
15,12. 15,12-14 etiami o esika ya yango te. Nsima ya mol. mwa 11 basengeli kotanga mol. mwa 15. Eyano ya Yawe ekomami o 19-21.
16,2. Na bomoi bwa ye mobimba Yeremia akolakisa bosolo bwa mateya ma ye. Amituni : ‘Ntina nini nabala mpe nabota ? Etikali moke baboma bato banso na mompanga. Tosala nde lilaka mpo ya moto awei sikawa, awa toyebi ’te moto moko akozala te mpo akunda baye banso bakokufa ?
17,14. Awa mpe Yeremia afungoli motema mwa ye, mpe ayebisi mpasi eye azali koyoka (18) ; amimoni lokola moto azoki, moto wa malali, oyo Nza­mbe akoki kobikisa. Ata alingi ya­ngo te, asengeli kosakola makambo ma mpa­si, maye makosalema noki te ; ya­ngo wana bato baseki ye. Bongo asengi ’te etumbu ekwela baye banyokoli ye. Akoki kosala lokola Yezu te, oyo abakemi o kuruse mpe asengi Tata wa ye ali­mbisa banyokoli ba ye (Lk 23,34).
18,1. Lokola mosali mbeki aboli mbeki eye abongisi malamu te, mpe asali mbeki esusu (tala mpe Iz 29,16 ; 30,14 ; 41,25 ; 45,7 ; Yer 19,1 ; 19,11 ; Rom 9,21), se bongo Nzambe akonyata bato baboyi ye, kasi akosala bango malamu soko babongoli mitema.
18,19. Awa mpe Yeremia asololi na Nza­mbe na losambo, mpo ayeba mpasi ya ye.
20,7. Yeremia ayebisi Nzambe lisusu mpasi ye azali koyoka ; etikali moke afunda Nzambe oyo atindi ye mosala moleki makasi ma ye. Nzokande na nsima (13) alobi ’te azali se kotia motema na ye : ata boni, Nzambe akobikisaka bato bazali kobunda na mpasi.
21,1. Mokonzi Sedekia atombokeli mokonzi wa Babilon ; yango wana akomi kobanga mpo basoda ba Babilon bakomi penepene ; asengi Yeremia atuna Yawe o nkombo ya ye. Yeremia ayanoli ye polele : Tika kobunda, tosa mokonzi wa Babilon, okokufa te ; kasi soko osali bongo te, bato mingi bakokufa. Kasi Sedekia akoyoka profeta te (32-39).
22,10. Mokonzi malamu Yozia akufaki o etumba o 609 (2 Bak 23,29) ; Yoakaz oyo akitani ye azalaki mokonzi bobele sanza isato o Yeruzalem, mpo bakambi ye o boombo o Babilon (2 Bak 23,30-34).
22,13. Yoyakim azalaki mwana mosusu wa mokonzi Yoakaz ; ayangeli Yeruzalem banda 609 tee 598 (2 Bak 23,34 - 24,6). Yeremia apaleli ye mpo ya makambo ma ye ma lipombo, mpe mpo apangani na babola mpe na bato ba mawa te.
22,24. Bokonzi bwa Konia (oyo babengaki mpe Yoyakin) boumeli mpe bobele sanza isato (597), mpo bakambi ye o boombo na bato basusu ba Yuda.
23,1. Balakisi bakengeli ba ekolo lokola bakengeli balingi etonga ya bango te. Tala mpe Yer 2,8 ; 10,21 ; 22,22 ; Ez 34,2 ; Za 10,3 ; 11,4-17 ; 13,7 ; Yo 10,1-21.
23,3. Na maloba ma bobondi Yeremia asakoli ’te mokolo moko baye banso bakangemi o boombo (o 597) bakozonga o ekolo ya bango.
23,5. Elako eye ekomami mpe o 33,15-16 : elikya ezali ’te mokolo moko nkoko wa Davidi akozala mokonzi ; ye moto wa bosembo, leka nkoko wa ye. O esika tokomi ‘mwana’ ekomami ‘eboto’ : balakisi Mesiya na nkombo eye. Tala mpe Iz 4,2 ; Za 3,8 ; 6,12.
23,9. O eteni eye Yeremia apaleli bakambi ba makambo ma Nzambe : ayoki mabe mpenza na bomoni ’te basali mosala mwa bango malamu ata moke te, mpo bakolandaka se makambo ma bango moko ; baprofeta baye profeta atangi awa bazali bato ba lokuta, baye Nzambe abyangi te : mosala mwa ba­ngo se kokosa bato !
23,33. Nzambe alobi na Yeremia : baprofeta ba lokuta na banganga Nzambe baye bakomitunaka, na boseki Nzambe soko profeta akosakola mpasi nini ekoki kokwela bango, mpe soko mokumba nini alingi kotiela bango lisusu. Yeremia asengeli koyebisa bango ’te maloba ma Nzambe mazali mokumba te ; kasi ba­ngo bakotielaka bato mokumba na ezalela ebe ya bango.
24,1. Ezalaki o 597 (2 Bak 25,27-29 ; Yer 22,24) ; balakisi bato baye bakei o boombo lokola ekoló ya figi ilamu, mpo bakokanisa Yawe (5-7), mpe baye batikali o Yeruzalem bazali lokola ekoló etondi na figi ibe, baye Yawe akoboya mpo bakokoba kosala mabe (8-10).
25,1. O eteni eye tokoki kotanga na bokuse makambo manso Yeremia ateyaki, na mpe likambo lisusu : boombo bokoumela mibu 70, se lokola bomoi bwa moto ; na nsima etumbu ekokwela mokonzi na ekolo ya Babilon.
25,9. Bapagano mpe bakoki kosalela Nzambe o mikano mya ye, ata bango moko bayebi te.
26,1. O 7,1-15 totangi ’te bakobebisa Tempelo ; awa (4-6) bakomi yango lisusu, kasi bayebisi mpe ’te bayangeli na bato banso baboyi maloba ma Yeremia mpe baluki koboma ye. Nsima (o 36,5) tokotanga ’te bapekisi ye koteya lisusu.
26,18. Tala Mi 3,12.
27,2. Ezali mabaya makangemi na mikulu maye bazalaki kotia o nkingo ya ngombe mpo ebenda epasoli to likalo : ezali elembo ya boombo.
27,3. Mbele mokonzi Sedekia abe­ngaki bango mpo ’te bayokana o etu­mba na mokonzi wa Babilon. Kasi Yeremia alobi ’te soko bameki kosala yango, bakokangema o boombo o ekolo ya bampaya.
28,17. Likambo liye Yeremia ayebisi lisalemi noki, yango elakisi ’te azali profeta wa solo. Tala 20,6 ; 29,32 ; 44, 29-30 ; 45,5 ; Mbk 18,21.
29,1. Baye Nabukodonozor aka­mbaki o boombo o 597 mpo ya kokebisa ba-Yuda ’te batomboko lisusu te, soki te, akotinda ’te babebisa Tempelo mobimba (tala 22,24 ; 24,1). Yeremia akomeli bango ’te bamikosa te : o Babilon bakoumela mingi.
29,10. Eleko elai lokola bomoi bwa moto (25,11).
30,1. O bit. 30-31 bakomi ’te bato ba­zali o boombo bakoki kobomba eli­kya ’te bakozonga, liboso ba-Samaria baye bakangemaki o 721, na nsima bato ba Yuda baye bakangemaki o 597 mpe o 587. Yango wana bakotangaka biteni biye ‘buku ya bobondi’, se lokola Iz 40-55. Solo, Nzambe asengelaki kotumbola bato ba ye, kasi mpasi eye ekopetola bango.
30,22. Na maloba maye Nzambe akati bondeko na ekolo ya ye (Lv 26,12 ; Mbk 29,12) ; kasi Yeremia ayebisi ’te Nzambe azalaki kolengele Bondeko bwa Sika (7,23 ; 11,4 ; 31,1 ; 31,31-33 ; 32,38 ; Bob 11,20).
31,3. Awa totangi maloba ma ekolo ya ba-Israel : balakisi yango lokola mwasi wa bolingo wa Mokonzi, oyo amilakisi na ye o eliki.
31,15. –Rama : mboka eke o Nordi ya Yeruzalem, esika bakundi Rakele, mwasi wa Yakob (1 Sam 10,2).
–Rakele : mama wa Yozefu (oyo abotaki Efraim na Manase) mpe wa Benyamin. Mabota maye bafandaki o kati ya ekolo. Yeremia alobi awa ’te Rakele aleli bana ba ye bakangemi o boombo mosika.
31,22. Mbala mingi Israel azalaki lokola mwasi oyo akimaki mobali wa ye (Yawe) ; sikawa Israel akozongela Mokonzi na motema moko, lokola mwasi akolukaka kosepelisa mobali wa ye.
31,29. Na eyele eye balakisi ’te bakopesa mbano na moto malamu, mpe etumbu na moto mabe ; bakanisi ’te etumbu ekokwela bana mpo ya mabe ma baboti, mpe libota mobimba mpo ya mabe ma moto moko. Yeremia alobi ’te ezali bongo te : etumbu ekokwela bobele mosumuki ye moko. Ezekiel mpe alimboli yango mingi o 14,12-20 ; 18,1-32 ; 33,12-20.
31,31. Milongo miye (31-34) kitoko mizali na ntina enene : Yeremia ayebisi ’te bato babukaki mbala mingi bondeko boye Nzambe akataki na bango ntango abimisaki bango o Ezipeti ; kasi alingi kokata na bango bondeko bwa sika ; bondeko boye bokopikama o bolingi bwa solo. Tokoki komono ’te basili kolakisa awa bondeko bwa sika mpe bwa seko, boye Yezu Kristu akoyela bato banso mpe bokopikama o mobeko mwa sika mwa bolingani (Ez 36,26-27 ; Ebr 8,6-12).
31,38. Bakotonga engumba lisusu, eye bato ba Babilon bakomisi mopotu (Ne 3-4).
32,2. Mbele bakangaki Yeremia mbala 2 to 3 o boloko. Awa totangi ndenge mokonzi Sedekia akangisi ye mpo ya maloba ma ye ma bokaneli. Tosili totangi maloba maye o 21,3-7, mpe tokotanga mango lisusu o 34,2-3. Mbele babimisi ye nsima ya mwa mikolo ; kasi akokangema lisusu (37,11-21 na 38,6-13).
32,8. Ntango ba-Babilon bayei kobundisa Yeruzalem, Nzambe ayebisi Yeremia ’te asomba elanga o Anatot, mboka ya bankoko ba ye ; alakisa bo­ngo ’te boboto bokozonga o ekolo.
32,12. Baruk azalaki moninga wa motema wa Yeremia (36,4-32 ; 43,3-7 ; 45,1-5). Yeremia akosomba elanga eye o miso ma banzeneneke baike mpo ya kopesa bango banso elembo ’te bato bakozwa bolamu lisusu.
33,1. O ntango bato ba Babilon bazalaki kobebisa ekolo ya Yuda, Yeremia abwaki miso epai ya maye makoya nsima, yango wana asangeli bato ’te Nzambe akolimbisa bango (1-13), mpe nkoko wa Davidi akosala makambo manene (14-18) ; yango ekosalema mpo ya bondeko boye Nzambe akataki na Davidi (19-25).
34,7. Bingumba biye bizalaki o Sudi-Westi ya Yeruzalem ; bato ba Babilon babotoli byango bobele na mpasi.
34,8. Bato ba nkita balakelaki ’te bakopesa baombo bonsomi soko basalisi bango mpo ya kobatela engumba. O ntango ena ba-Babilon batiki kobundisa Yeruzalem mwa mikolo, mpo ya basoda ba Ezipeti bazalaki koya pene na bango (37,11). Yango wana bankolo bakanisi ’te banguna bakozonga lisusu te, mpe basusu baboyi kokokisa elako ya bango ya bopesi baombo bonsomi.
34,14. Tala Mbk 15,12-13.
34,18. Bazalaki koleka o kati ya biteni bya nyama eye babomaki mpo ya kolakisa ’te bandimi makambo mabe makangemi na nyama ena makoki ko­kwela bango soko bakokisi bilako bya bango te.
35,2. Ba-Rekab bazalaki mwa li­ngomba lya bato babuneli makambo ma Yawe ; balingi ’te bakoba kofanda o biema lokola bankoko o eliki, bakata bilanga te, bamele vino te. Nkoko ya bango Yonatan moto alayisi bango ’te basala bongo (2 Bak 10,15). Yango wana baboyi komele vino lokola Yeremia asengi bango.
36,1. Baruk, moninga mpe mokomi wa Yeremia, atikeli biso bikela na maloba maike ma Yeremia o ntango ya Yoyakim (609-597) mpe ya Sedekia (597-587) : alakisi mingi ndenge banyokoli Yeremia.
36,5. Bayangeli ba Tempelo bapekisaki Yeremia koteya polele, mpo balobaki ’te na mateya ma ye azalaki kolembisa bato mitema.
36,19. Mokonzi Yoyakim atindaki bankumu ’te bayebisa ye maye Yeremia azalaki kosala, kasi bandimeli Yeremia na Baruk, yango wana bapesi bango lilako liye.
37,12. Tala 32,7-16 ; ezali se elanga eye etangemi wana.
37,17. Mokonzi Sedekia abeti nte­mbe : atia motema na maloba ma Yeremia, to aboya malako ma bato ba ye.
38,7. Mompaya oyo wa motema ngolu akozwa mbano na bolamu boye Yeremia alakeli ye (39,15-18).
39,1. O 2 Bak 25,1-21 basololi mpe likambo liye lya mpasi likweli ba-Yuda.
39,4. O Araba, ekolo pene na Yordane mpe na Mbu ya Liwa, nzete izalaki mingi te.
39,14. Akikam abatelaki Yeremia (26,24) ; mokonzi wa Babilon akotia mwana wa ye, Godolia, moyangeli wa Yudea.
40,1. Bayebisi awa ndenge nini esalemi mbala ya ibale ’te Yeremia akei o boombo te. O 39,11-14 ekomami ndenge esalemi mbala ya liboso.
41,5. Ata Tempelo ebukani, bazalaki kobonza mabonza o libanda.
42,1. Bato ba ekolo babangi ’te bato ba Babilon bakobekola liwa lya moyangeli Godolia, yango wana bato mingi bakei o Ezipeti.
43,7. Tapanes ezalaki engumba o ndelo ya Nordi ya Ezipeti (2,16).
44,30. Yeremia asili ayebisi ’te Farao Ofra akobomama na Amasis, moto wa Libya.
45,1. Nzambe atindi Yeremia asa­ngela Baruk ’te akobika, o ntango ya makama mingi. O ntango ena Yeremia azalaki o Ezipeti te, kasi mwa moke liboso ’te bato ba Babilon babotolo Yeruzalem.
52,1. O nsuka ya buku Yeremia makambo makomami mwa kilikili : bata­ngi lisusu maye masili makomami. O 52,28-29 bayebisi motango mwa baye bakambami o boombo.