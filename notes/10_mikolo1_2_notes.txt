MIKOLO 1


2,1. O kati ya bankoko ba Abarama mokomi atangi bobele Izaka na Ismael (1,28), mpe o kati ya bankoko ba Izaka atangi Ezau na Yakob. O kati ya bana 12 ba Yakob atangi nkoko moko moko wa Yese, mpo Davidi, mokonzi wa Israel akobotama o libota lya ye (2,15).
3,1. Tokoki kokamwa mpo mokomi alingaki kotanga mabota 12 ma Israel na bituka binene bya bango ba bileko bya kala (4,1.24 ; 5,1.11.23 ; ...), nzokande ayebisi molongo mwa bakonzi kin’o boo­mbo (3,10-16), mpe bankoko basusu ba Davidi nsima ya boombo (3,17-24). Mbele asali bongo mpo amoni ntina enene ya kolakisa Davidi.
5,26. Ntango Teglat-Falasar akomi mpe mokonzi wa Babilon, akamati nkombo ya ibale, Pul (797 liboso lya Y.K.). Mbala mwa mingi akei kobundisa bikolo o Esti mpe o Westi ya Yordane, mpe akangisi bato ba bango o boombo (2 Bak 15,19.29).
8,1. Tosili kotanga molongo mwa ba­nkoko ba Benyamin o 7,6-12. Mokomi apesi awa molongo moye lisusu, kasi na bokeseni. Mpo nini apesi nkombo iye lisusu ? Mpo bato mingi ba Benyamin bakoya kofanda o Yeruzalem (8,15-28 ; 
9,7-9) ; alingi mpe kolakisa libota lya 
Saul, mokonzi wa yambo wa Israel, oyo azalaki moto wa libota lya Benyamin.
9,1-34. Nkombo mingi itangemi o eteni eye ya 9 ikoki kotangema mpe o Ne 11 : ezali molongo mwa bato bafandi o Yeruzalem nsima ya boombo. Mpo ya kolakisa ’te ezali engumba esantu, bakomi mingi nkombo ya banganga Nzambe, 
ba-Levi, ‘babonzami’ na basaleli basusu ba Tempelo.
9,1. Toyebi te buku nini etangemi awa ; kasi ezali 1 Bak to 2 Bak te !
9,35-44. Eteni ya yambo ya buku eye esuki awa : mokomi atangeli biso milo­ngo mya bankoko banda Adamu kin’o Abarama ; na nsima atangi mabota 12, kasi mingi mpenza mabota Yuda mpe ma Benyamin, libota lya Levi mpe bato ba Yeruzalem. Sikawa akosolola na bokuse nsuka ya Saul, mokonzi wa yambo wa Israel. Na nsima akobanda lisolo lilai litali Davidi.
10,1-14. Banda awa mokomi apesi biteni binene bya buku ibale ya Samuel mpe ya Bakonzi. O buku ina okoki kotanga ndimbola mwa mingi ya maye makomami o buku eye.
12,1. Molongo moye mwa baye bala­ndi Davidi liboso ’te akoma mokonzi (12,1-23) mokokisi moye mokomami o buku ya Samuel. Na nsima apesi molo­ngo mwa bayangeli ba mabota 12 baye baluboli Davidi mokonzi wa Israel mobimba.
15,1-24. Awa mokomi akokisi lisolo liye likomami o 2 Sam 6,2-19 : alakisi mosala mwa banganga Nzambe na ba-Levi o ntango bakei kotika Sanduku ya Bo­ndeko o Engumba ya Davidi, mosala mwa bayembi mpe mwa baye babeti miziki. Asali yango mpo ’te banso batosa miti-ndo mya milulu miye.
16,7. Nzembo eye ezali na biteni ya Nz 105, Nz 96 na Nz 106. Mokomi alakisi manso Davidi asali mpo ya kobongisa milulu mya losambo.
16,39. Bazalaki kokumisa mingi Ga­baon, mwa o Nordi ya Yeruzalem, mpo o esika eye bazalaki na Ema eye Moze atongaki mpo ya kobomba Sanduku ya Bondeko o eliki (1 Mkl 21,29 ; 2 Mkl 1,3). Davidi atongisi Ema ya sika mpo ya kobomba Sanduku o Yeruzalem (16,1). O ntango ya Salomo naino Gabaon ekotikala ndako esantu ya liboso (1 Bak 3,4-15) tee bakosilisa kotonga Tempelo o Yeruzalem.
17,1-15. Awa tokotanga lisusu maloba ma profeta Natan (2 Sam 7), mpo ezalaki na ntina enene : yango elakisi bondeko Nzambe akati na Davidi, mpe ndenge bokonzi bwa ye bokoumela. Bakoyebisa mpe bato ba libota lya Davidi makambo mingi matali boyei bwa Mesiya.
18,1-13. Awa mokomi akutoli mingi maye matali boyangeli bwa Davidi 
(o 2 Sam 9 - 1 Bak 2) ; atangi bobele ’te alongaki bitumba biike, mpe atiki maye makoki kolakisa mbeba ya Davidi. Alakisi mpe ’te, mpo ya bitumba biike, Davidi akoki kotonga Tempelo te (22,8 ; 28,3), kasi biloko akobotolo bikosalisa mingi o botongi Tempelo (29,2-5).
22,9. ‘Shelomoh’ o ebrei euti na liloba Shalom (boboto), awa Davidi azalaki moto wa bitumba.
23,1-32. O biteni 23-26 mokomi apesi milongo milai mya ba-Levi, banganga Nzambe, bayembi na bakengeli engebene na misala mya bango. Toyebi te o esika nini azwi nkombo iye inso.
28,1. Maye mokomi ayebisi te : makambo ma kwokoso Davidi ayokaki nta­ngo akomi mobange, mpe ndenge Adonia aweli kokoma mokonzi na Salomo (1 Bak 1). O eteni ya 28 balakisi Davidi lokola moto abongisi makambo ma bokitani na kimia, apesi bankumu ba Israel malako, mpe asengi bango ’te bale­ndisa Salomo o mosala mwa botongi Te­mpelo, mpe batosa mibeko mya Mokonzi.
29,1. Davidi apesi biloko binso bya nkita, biye abotoli mpe biye bizalaki bya ye moko, mpo ya mosala mwa botongi Tempelo ; bato banene babakisi mpe biloko mingi.
29,10. O losambo loye kitoko Davidi andimi ’te binso bizali na bato biuti na Nzambe ; baye babonzi biloko mpo ya botongi Tempelo bazongiseli Nzambe eteni ya biye akabelaki bango. Awa babonzi byango na motema semba, Nza­mbe akosepela na byango.
29,22. O 23,1 basili basololi ndenge bapakoli Salomo mafuta ma bokonzi. Bapakoli ya mango mbala ibale te.



MIKOLO 2

1,1-17. Maye makomami awa masili makomami o 1 Bak 3,4-15. Mokomi asali bongo mpo ya kokoba lisolo lya ye.
1,18. Maye makomami o awa o 1,18- 2,17 maulani na maye makomami o 1 Bak 5,15-32 ; 7,13-14.
3,1. Awa o 2 Mkl 3-4 asololi bobele mambi manene ma botongi Tempelo, mpo apangani mingi na matongi te, kasi na milulu. Toyeba mpe ’te matongi maye mazalaki lisusu na nkembo te lokola o ntango Salomo atongaki Tempelo (960), eye babebisaki o mobu mwa 587.
9,29. Se lokola atangaki mbeba ya Davidi te, sikawa asololi mpe te ndenge, o ntango ya bobange bwa Salomo, basi ba ye babendi ye o nzela ya bokumisi ba­nzambe ba bampaya (1 Bak 11,1-9), makambo na banguna ba ye mpe te (1 Bak 11,14-40). Akomi bobele maye makoki kopesa ye lokumu.
10,2. Mokomi alakisi Yeroboam oyo bobele na bokuse, mpo alingi kokoma lisusu te maye masili makomami o 1 Bak 11, esika totangi ndenge atombokeli Salomo, mpe ndenge profeta Akia asakoli ’te akokoma mokonzi (1 Bak 11,26-39).
11,13. Mokomi atangi lisumu lya Yeroboam bobele na bokuse (14-15). O 1 Bak 11,29-39 totangi ’te asalaki bikeko bibale bya ngombe ya wolo, biye atii o tempelo ya Betel mpe ya Dan. O ntango ena banganga Nzambe na ba-Levi mingi bakei o Yeruzalem, mpo balingi koboya mokonzi na ndako esantu ya Yawe te.
12,5. Awa tokoki kotanga makambo masusu ma Semaya, maye makomami o 1 Bak te. Mbele balakisi mosala mwa profeta oyo o buku eye etangemi o 12,15, eye ezalaki mpe na nkombo ya ye.
13,1. Ezali Abiam oyo atangemi o 1 Bak 14,31 ; 15,1.7.8.
13,4. Maloba maye ma Abia (13,4-12) makomami o buku ya Bakonzi te. Abia alingi kolakisa ’te bobele Yuda azali na Nzambe mpe na mokonzi wa solo ; bobele o Yuda bonganga Nzambe mpe milulu mya losambo malongobani na mibeko mya Moze.
14,8. Toyebi Zera oyo te ; atangemi o 1 Bak mpe te. Mbele moto oyo wa Etiópia (Kus) azalaki komanda wa basoda ba Ezipeti baye farao Sesonk atikaki o eteni ya Sudi ya ekolo (12,2-3). Kasi Kus ezali mpe nkombo ya bato baye bafandaki o eliki ya Negeb mpe bakotelaki mokili mwa Yuda.
15,1. Asa asali makasi mpo ya kobo­ngisa makambo ma Nzambe, leka maye makomami o 1 Bak 14. Profeta Azaria alendisi ye o mosala moye. Okoki mpe kotanga ndenge Ezekia (2 Mkl 29 na 32) na Yozia (2 Mkl 34 na 35) bamipesi na mosala moye.
16,7. Mokomi abakisi likambo likomami o  1 Bak 15 te : Kanani alingi te ’te basenge bampaya lisalisi. Na nsima Izaya akosala se bongo (Iz 30,1-7 ; 31,1-3), mpo ateyaki ’te basengeli kotia mitema na Yawe, na ba-Ezipeti te.
16,14. Batumbi ebembe ya mokonzi te (likambo lya nsomo o miso ma ba-Israel : tala Am 2,1) ; kasi batumbeli mokonzi mpaka, lokola bazalaki kosala ntango ba­kundi bakonzi baye balakisaki Nza­mbe 16,7. Mokomi abakisi likambo likomami o  1 Bak 15 te : Kanani alingi te ’te basenge bampaya lisalisi. Na nsima Izaya akosala se bongo (Iz 30,1-7 ; 31,1-3), mpo ateyaki ’te basengeli kotia mitema na Yawe, na ba-Ezipeti te.
17,1-2. Mokomi alingi kopesa Yozafat lokumu lonene, lokola tata wa ye Asa. Na maye makomami o 1 Bak 22,41-51 abakisi makambo masusu : azalaki komemya Mobeko mingi (17,3-9), mpako eye bikolo bisusu biyelaki ye (17,10-11), ndenge abongisi limpinga lya basoda (17,2-19).
19,2. Profeta oyo atangemi o buku ya Bakonzi te. Mokomi ayebisi maloba ma ye : Yozafat abungi ntango aluki boyokani na Akab ; nzokande Nzambe ayokeli ye ngolu, mpo asalaki mpe malamu mi­ngi.
19,4-11. Mokomi akumisi Yozafat na mosala mosusu monene (moye mokomami o buku ya Bakonzi te) : abongisi makambo ma kosambisa bato : bazuzi bakoki kotala bilongi bya bato te (19,6-7).
20,1-30. O buku ya Bakonzi batangi etumba eye te, kasi mokomi alingi koyebisa yango mpo alakisa bongo ndenge Yozafat azalaki kotia motema na Yawe. Mpe Yawe akondimela ye.
21,12. Profeta Elia atangemi se mbala yoko o buku eye, mpo bikela bya ye mingi bikomami o buku ya Bakonzi.
21,19. Tala 16,14+ : Bato banso batali bokono bwa nsomo bwa Yoram lokola etumbu ekweli moto oyo wa motema mabe.
24,17. Ekomami o buku ya Bakonzi te ndenge Yoas abebi. Mbele bakambi ba makambo ma banzambe ba bampaya bazwaki nzela ya komilakisa mingi nsima ya liwa lya nkumu wa banganga Nza­mbe.
24,22. Zakaria oyo ezali oyo Yezu atangi (Mt 23,35).
25,4. Tala Mbk 24,16.
25,5. Leka maye makomami o 2 Bak 14,7, bayebisi awa ndenge babongisi li­mpinga lya basoda ba bango, mpe etu­mba babundi na ba-Edom.
26,9. Ozia atongi bizibeli mpe bifelo lisusu, biye Yoas abebisaki (25,23).
26,18. O ntango ya kala bakonzi ba ekolo bakokaki kosala misala misusu mya banganga Nzambe, kasi na nsima bobele banganga Nzambe bakokaki kotumba mpaka (1 Mkl 23,12).
26,19. Etumbu eye ekwelaki mpe Miriam, mpo aboyaki maloba ma ndeko wa ye Moze (Mit 12,10). Moto akomi na mbindo mpo ya maba akoki kofanda o kati ya bato basusu te (Lv 13,46).
28,9. Ata mokomi alingi kopesa bato ba ekolo ya Nordi lokumu mingi te, apesi lisolo liye lya profeta wa Samaria oyo atangi bato ba Yuda bandeko ba ye mpe asengi bakonzi ba Israel ’te bazongisa bonsomi na bato bakangaki.
29,5. O biteni 29 tee 31 tokoki kota­nga ndenge Ezekia abongisi makambo ma bokumisi Nzambe ; o 2 Bak 18,4 batangi yango na bokuse. Mpo mokonzi Akaz akangisaki Tempelo mpe apekisaki milulu mya losambo (28,14), Ezekia asengeli kopetola Tempelo, kobonza libonza lya bofuti masumu (29,23) mpe kozongisa momeseno mwa libonza lya mokolo na mokolo (29,35).
30,9. Mokomi ayebisi te ’te ba-Asur babomi bokonzi bwa Nordi mpe bwa Samaria (721 liboso lya Y.K.), na mpe ndenge bato ebele bakendeki o boombo (2 Bak 17,5-6). Kasi alakisi ndenge bato ba Yuda babosani ba-Israel o boombo te. O ntango makambo maye makomami bazalaki naino na elikya ’te baye banso bakangemi o boombo bakoka kozonga o ekolo ya bango.
32,1. Mokomi apesi lisolo lya Senekarib oyo ayei kobundisa Yeruzalem (2 Bak 18,13-16) na bokuse : moto wa boyambi bwa solo, Ezekia, alendisi bato ’te babwaka elikya te.
32,29. Ezekia akomi na nkita enene ; yango elakisi ’te Nzambe abenisi ye, lokola David (1 Mkl 29,2-4) na Salomo (2 Mkl 9,13-28).
33,11. O 2 Bak bayebisi te ndenge Manase akambamaki o boombo mpe abo-ngoli motema nsima. Toyebi ’te moko­nzi wa Asur abengisi bayangeli ba bikolo bya ye binso epai ya ye ; toyebi te soko mokomi oyo atalaki yango lokola Ezekia akangemaki o boombo.
34,3. Lokola o 2 Bak 23,4-20, awa mpe basololi mingi ndenge Yozia abongisi makambo ma bokumisi Nzambe ; yango esalemi mpe mpo bazwi buku ya Mobeko lisusu (34,8-18). Tala mpe 2 Bak 22,2+.
36,1. Makambo malandi nsima ya liwa lya Yozia makomami awa na bokuse. Mbele mokomi alingaki kokoma makambo maye ma mpasi (nsuka ya Yeruzalem mpe boombo) na bolai te.
36,22. O nsuka ya 2 Bak totangaki maloba ma elikya (mpo ba-Asur balimbisi Yoyakin). O buku eye ekomami nsima bayebisi na esengo ’te mokonzi wa Persi, Siro, akobimisa etinda mpo ya kolingisa ba-Yuda bazonga o ekolo ya bango. Tokoki kotanga milongo mya nsuka (22-23) mpe o Esd 1,1-3.
